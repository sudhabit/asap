<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'admin';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'admin';
//For Basicsettings
$route['editbasicsettings'] = 'Basicsettings/editbasicsettings';
//For Category
$route['viewCategory'] = 'Category/viewCategory';
$route['addCategory'] = 'Category/addCategory';
$route['editCategory'] = 'Category/editCategory';
//For Sub Category
$route['viewSubcategory'] = 'Subcategory/viewSubcategory';
$route['addSubcategory'] = 'Subcategory/addSubcategory';
$route['editSubcategory'] = 'Subcategory/editSubcategory';
//For Vendor
$route['viewVendor'] = 'Vendor/viewVendor';
$route['addVendor'] = 'Vendor/addVendor';
$route['editVendor'] = 'Vendor/editVendor';
// For Country
$route['viewcountry'] = 'Country/viewcountry';
$route['addcountry'] = 'Country/addcountry';
$route['editcountry'] = 'Country/editcountry';
// For State
$route['add_state'] = 'State/add_state';
$route['viewstate'] = 'State/viewstate';
$route['editstate'] = 'State/editstate';
// For District
$route['addDistrict'] = 'District/addDistrict';
$route['editDistrict'] = 'District/editDistrict';
$route['viewDistrict'] = 'District/viewDistrict';
//For City
$route['viewcities'] = 'City/viewcities';
$route['add_city'] = 'City/add_city';
$route['editcity'] = 'City/editcity';
// For Pincode
$route['addpincode'] = 'Pincode/addpincode';
$route['editpincode'] = 'Pincode/editpincode';
$route['viewpincode'] = 'Pincode/viewpincode';
// For Locations
$route['addLocations'] = 'Location/addLocations';
$route['editLocation'] = 'Location/editLocation';
$route['viewLocations'] = 'Location/viewLocations';
// For Cusine Types
$route['addCusinetype'] = 'Cusinetypes/addCusinetype';
$route['editCusinetype'] = 'Cusinetypes/editCusinetype';
$route['viewCusinetype'] = 'Cusinetypes/viewCusinetype';
//For Product Weights
$route['addProductweights'] = 'Productweights/addProductweights';
$route['editProductweights'] = 'Productweights/editProductweights';
$route['viewProductweights'] = 'Productweights/viewProductweights';
//For admin Login and logout
$route['adminLogin'] = 'Admin/adminLogin';
$route['logout'] = 'admin/logout';
//For vendor Login,logout and details
$route['vendorLogin'] = 'Mastervendor/vendorLogin';
$route['vendorlogout'] = 'Mastervendor/logout';
$route['Vendordetails'] = 'Vendordetails/Vendordetails';
//For produt Addons
$route['addProductaddon'] = 'Productaddon/addProductaddon';
$route['viewProductaddon'] = 'Productaddon/viewProductaddon';
//For vendor Products
$route['addVendorproducts'] = 'Vendorproducts/addVendorproducts';
$route['editVendorproducts'] = 'Vendorproducts/editVendorproducts';
$route['viewVendorproducts'] = 'Vendorproducts/viewVendorproducts';

// For Branch in vendor panel
$route['addBranch'] = 'Branch/addBranch';
$route['editBranch'] = 'Branch/editBranch';
$route['viewBranch'] = 'Branch/viewBranch';
//For branch Login,logout and details
$route['branchLogin'] = 'Masterbranch/branchLogin';
$route['branchlogout'] = 'Masterbranch/logout';
$route['branchdetails'] = 'Branchdetails/branchdetails';
// For Product Price in Branch panel
$route['addProductprice'] = 'Productprice/addProductprice';
$route['editProductprice'] = 'Productprice/editProductprice';
$route['viewProductprice'] = 'Productprice/viewProductprice';


//service routing

$route['api/login'] = 'api/AsapService/loginUser';
$route['api/signup'] = 'api/AsapService/registerUser';
$route['api/otpsend'] = 'api/AsapService/sendOtp';
$route['api/otpcheck'] = 'api/AsapService/verifyOtp';
$route['api/category'] = 'api/AsapService/categories';
$route['api/changePassword'] = 'api/AsapService/changePassword';
$route['api/setpassword'] = 'api/AsapService/resetPassword';
$route['api/restaurant'] = 'api/AsapService/restaurantList';
$route['api/menu'] = 'api/AsapService/restaurantItemList';
$route['api/profile'] = 'api/AsapService/userProfile';
$route['api/profileupdate'] = 'api/AsapService/updateProfile';
$route['api/itemList'] = 'api/AsapService/itemList';
$route['api/itemDetailList'] = 'api/AsapService/itemDetailList';
$route['api/banner'] = 'api/AsapService/bannerList';
$route['api/addressList'] = 'api/AsapService/addressList';
$route['api/addAddress'] = 'api/AsapService/addAddress';
$route['api/editAddress'] = 'api/AsapService/editAddress';
$route['api/makedefault'] = 'api/AsapService/defaultAddress';
$route['api/userList'] = 'api/AsapService/userList';
$route['api/addgroup'] = 'api/AsapService/addgroup';
$route['api/addgrouplist'] = 'api/AsapService/addgrouplist';
$route['api/groupDetail'] = 'api/AsapService/groupDetail';
$route['api/groupPrimary'] = 'api/AsapService/groupPrimary';
$route['api/deleteUserFromGroup'] = 'api/AsapService/deleteUserFromGroup';
$route['api/saveorderList'] = 'api/AsapService/saveorderList';
$route['api/myOrders'] = 'api/AsapService/myOrders';
$route['api/orderDetail'] = 'api/AsapService/mySingleOrders';
$route['api/groupList'] = 'api/AsapService/groupList';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
