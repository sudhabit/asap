<?php
include_once'header.php';
?>
<div class="page-content-wrapper ">
	<div class="container-fluid">
		<div class='row'>  
			<div class="col-md-12 col-xl-12">
				<div class="card m-b-30">
		            <!--      Wizard container        -->
		            <div class="wizard-container">
		                <div class="card wizard-card" data-color="red" id="wizard">
							<form class="mb-0" action="<?php echo base_url() ?>addBranch" 
                            method="POST" enctype="multipart/form-data">
		                <!--        You can switch " data-color="blue" "  with one of the next bright colors: "green", "orange", "red", "purple"             -->
                                <?php
                                    echo $this->session->flashdata('msg');
                                ?>
		                    	<div class="wizard-header">
		                        	<h3 class="wizard-title m-l-30">
		                        		Add Branch
		                        	</h3>
									
		                    	</div>
								<div class="wizard-navigation">
									<ul>
			                            <li><a href="#details" data-toggle="tab">Basic information</a></li>
			                            <li><a href="#captain" data-toggle="tab">Business Information</a></li>
			                            <li><a href="#description" data-toggle="tab">Account Detail</a></li>
			                        </ul>
								</div>

		                        <div class="tab-content">
		                            <div class="tab-pane" id="details">
									
									
										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="inputEmail4" class="bmd-label-floating">User Name</label>
												<input type="text" name='username' class="form-control"  required>
											</div>
											<div class="form-group col-md-6">
												<label for="inputPassword4" class="bmd-label-floating">password</label>
												<input type="password" name='password' class="form-control" required>
											</div>
										</div>
										<div class="form-row">
                                            <div class="form-group col-md-6">
												<label for="inputPassword4" class="bmd-label-floating">Branch Address</label>
												<textarea id="autocomplete3"  name='branch_adress' class="form-control" maxlength="225" rows="3" placeholder=""></textarea>
											</div>
										</div>
										<input type="hidden" name='latitude' id='latitude' class="form-control">
										<input type="hidden" name='longitude' id='longitude' class="form-control">
		                            </div>
                                    <div class="tab-pane" id="captain">
		                                <div class="form-row">
											<div class="form-group col-md-6">
												<label for="inputEmail4" class="bmd-label-floating">Delivery Time(in mns)</label>
												<input type="number" name='delivery_time' class="form-control">
											</div>
											<div class="form-group col-md-6">
												<label for="inputPassword4" class="bmd-label-floating">Min Order</label>
												<input type="number" name='min_order' class="form-control">
											</div>
										</div>
										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="inputEmail4" class="bmd-label-floating">Delivery Charges</label>
												<input type="number" name='delivery_charges' class="form-control">
											</div>
                                            <div class="form-group col-md-6 clockpicker">
												<label for="inputEmail4" class="bmd-label-floating">Opening Time</label>
												<input type="text" name='opening_time' class="form-control">
											</div>
										</div>
										<div class="form-row">
											<div class="form-group col-md-6 clockpicker">
												<label for="inputEmail4" class="bmd-label-floating">Closing Time</label>
												<input type="text" name='closing_time' class="form-control">
											</div>
										</div>
		                            </div>
                                    
		                            <div class="tab-pane" id="description">
		                                <div class="form-row">
                                            <div class="form-group col-md-6">
												<label for="inputEmail4" class="bmd-label-floating">Bank Name</label>
												<input type="text" name='bank_name' class="form-control">
											</div>
                                            <div class="form-group col-md-6">
												<label for="inputEmail4" class="bmd-label-floating">Account Number</label>
												<input type="text" name='account_number' class="form-control">
											</div>
										</div>
										<div class="form-row">
                                            <div class="form-group col-md-6">
												<label for="inputEmail4" class="bmd-label-floating">IFSC Code</label>
												<input type="text" name='ifsc_code' class="form-control">
											</div>
											<div class="form-group col-md-6">
												<label for="inputPassword4" class="bmd-label-floating">Bank Branch Name</label>
												<input type="text" name='bank_branch_name' class="form-control">
											</div>
										</div>
										<div class="form-row">
                                            <div class="form-group col-md-6">
												<label for="inputPassword4" class="bmd-label-floating">Bank Address</label>
												<textarea id="textarea"  name='bank_address' class="form-control" maxlength="225" rows="3" placeholder=""></textarea>
											</div>
                                            <div class="form-group col-md-6">
                                                <label for="inputPassword4" class="bmd-label-floating">Cancelled Cheque</label>
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div  style='display:none' class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
                                                    <div>
                                                    <span class="btn btn-info btn-file">
                                                        <span class="fileinput-new">Select image</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="cancelled_cheque">
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
										</div>
		                            </div>
		                        </div>
	                        	<div class="wizard-footer">
	                            	<div class="pull-right">
	                                    <input type='button' class='btn btn-next btn-fill btn-danger btn-wd' name='next' value='Next' />
	                                    <input type='submit' class='btn btn-finish btn-fill btn-danger btn-wd' name='finish' value='Finish' />
	                                </div>
	                                <div class="pull-left">
	                                    <input type='button' class='btn btn-previous btn-fill btn-default btn-wd' name='previous' value='Previous' />

										
	                                </div>
	                                <div class="clearfix"></div>
	                        	</div>
		                    </form>
		                </div>
		            </div> <!-- wizard container -->
		       </div> <!-- end col -->
		</div> <!-- end row -->
	</div>							
	</div>
</div>
	   <?php
	   include_once'footer.php';
	   ?>
	   <!--     Fonts and icons for wizard    -->
		<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
		<link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" />
		<link href="<?php echo base_url() ?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
		<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url() ?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>

	<!--  Plugin for the Wizard -->
	<script src="<?php echo base_url() ?>assets/js/material-bootstrap-wizard.js"></script>

	<!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
	<script src="<?php echo base_url() ?>assets/js/jquery.validate.min.js"></script>