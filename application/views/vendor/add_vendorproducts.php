<?php
include_once'header.php';
?>
<div class="page-content-wrapper ">
    <div class="container-fluid">
        <div class='row'>  
            <div class="col-md-12 col-xl-12">
                <div class="card m-b-30 m-t-30">
                    <div class="card-body">
                        <h4 class="mt-0 header-title">Add Vendor Products</h4>                 
                        <form action= "<?php echo base_url() ?>addVendorproducts" method="POST" enctype="multipart/form-data" class="mb-0">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                <label for="Productname" class="bmd-label-floating">Product Name</label>
                                <input type="text" class="form-control" name="product_name" required>
                                </div>
                                <div class="form-group col-md-6">
                                <label for="Productdescription" class="bmd-label-floating">Product Description</label>
                                <textarea id="textarea" class="form-control" name="product_description" maxlength="225" rows="3"></textarea>
                                </div>
                            </div>  
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                <label for="masterCategory" class="bmd-label-floating">Master Category</label>
                                    <select id="masterCategory" class="form-control mb-3 custom-select" name="master_category_id" required>
                                        <option value="">Select Master Category</option>
                                        <?php                    
                                            $count = count(array_filter($resultCnt));
                                            if($count > 0) {
                                            $i=0;
                                            foreach($resultCnt as $key => $row){
                                            ?>
                                                <option value="<?php echo  $row['id'] ?>"><?php echo  $row['master_category_name'] ?></option>
                                            <?php
                                            }
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                <label for="Category" class="bmd-label-floating">Category</label>
                                    <select id="catId" class="form-control mb-3 custom-select" name="category_id" required>  
                                      <option value="">Select  Category</option>
                                   </select>  
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                <label for="Subcategory" class="bmd-label-floating">Subcategory</label>
                                     <select id="subCatId" class="form-control mb-3 custom-select" name="sub_category_id">  
                                      <option value="">Select Subcategory</option>
                                   </select>
                                </div>
                                <div class="form-group col-md-6">
                                 <label for="Producttype" class="bmd-label-floating">Product Type</label>
                                    <select id="Producttype" class="form-control mb-3 custom-select"name="product_type">
                                        <option value="">Select Product Type</option>
                                        <option value="1">Veg</option>
                                        <option value="2">Non Veg</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="ProductImage" class="bmd-label-floating">Product Image</label>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div  style='display:none' class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
                                        <div>
                                            <span class="btn btn-info btn-file">
                                                <span class="fileinput-new">Select image</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="file" name="product_image" required>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                            <button type="submit" class="btn btn-raised btn-primary mb-0">Submit</button>
                        </form>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->
                                
    </div>
</div>
<?php
include_once'footer.php';
?>
                                       