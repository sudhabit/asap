<?php
include_once'header.php';
?>
<div class="page-content-wrapper ">
	<div class="container-fluid">
		<div class='row'>  
			<div class="col-md-12 col-xl-12">
				<div class="card m-b-30 m-t-30">
					<div class="card-body">
						<h4 class="mt-0 header-title">Add Cusine Types</h4>					
						<form action= "<?php echo base_url() ?>Cusinetypes/addCusinetype" method="POST" enctype="multipart/form-data" class="mb-0">
<?php echo $this->session->flashdata('msg'); ?>
							<div class="form-row">
								<div class="form-group col-md-6">
								<label for="inputCusinetype" class="bmd-label-floating">Cusine Type</label>
								<input type="text" class="form-control" name="cusine_type_name" required>
								</div>
							</div>			
							<button type="submit" class="btn btn-raised btn-primary mb-0">Submit</button>
						</form>
					</div>
				</div>
			</div> <!-- end col -->
		</div> <!-- end row -->
								
	</div>
</div>
<?php
include_once'footer.php';
?>
                                       