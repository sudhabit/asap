<?php
include_once'header.php';
?>
<div class="page-content-wrapper ">
	<div class="container-fluid">
		<div class='row'>  
			<div class="col-md-12 col-xl-12">
				<div class="card m-b-30 m-t-30">
					<div class="card-body">
						<h4 class="mt-0 header-title">Edit State</h4>					
						<form action= "<?php echo base_url() ?>State/updatestate" method="POST" enctype="multipart/form-data" class="mb-0">
							<div class="form-row">
								<div class="form-group col-md-6">
								<label for="inputCountry" class="bmd-label-floating">Country</label>
								    <select class="form-control mb-3 custom-select"  name="country_id" id="countryId" required>
								    	<option value="<?php echo $result['country_id']; ?>"><?php echo $result['country']; ?> </option> 
	                                     <option value="">select Country </option>
	                                     <?php 
	                                        foreach($country as $val)
	                                        {
	                                            echo '<option value="'.$val['id'].'">'.$val['country_name'].'</option>';
	                                        }
	                                     ?> 
                                    </select> 
								</div>
								<div class="form-group col-md-6">
								<label for="inputState" class="bmd-label-floating">State</label>
								<input type="text" class="form-control" name="state_name" value="<?php echo $result['state_name']; ?>" required>
								</div>
							</div>
							<input type="hidden"  name="id" value="<?php echo $result['id']; ?>">			
							<button type="submit" class="btn btn-raised btn-primary mb-0">Submit</button>
						</form>
					</div>
				</div>
			</div> <!-- end col -->
		</div> <!-- end row -->
								
	</div>
</div>
<?php
include_once'footer.php';
?>
                                       