<?php
include_once'header.php';
?>
<div class="page-content-wrapper ">
	<div class="container-fluid">
		<div class='row'>  
			<div class="col-md-12 col-xl-12">
				<div class="card m-b-30 m-t-30">
					<div class="card-body">
						<h4 class="mt-0 header-title">Add Form</h4>					
						<form class="mb-0">
							<div class="form-row">
								<div class="form-group col-md-6">
								<label for="inputEmail4" class="bmd-label-floating">Email</label>
								<input type="email" class="form-control" id="inputEmail4" >
								</div>
								<div class="form-group col-md-6">
								<label for="inputPassword4" class="bmd-label-floating">Password</label>
								<input type="password" class="form-control" id="inputPassword4" >
								</div>
							</div>
							
							<div class="form-group">
								<label for="inputAddress2" class="bmd-label-floating">Address </label>
								<textarea id="textarea" class="form-control" maxlength="225" rows="3" placeholder=""></textarea>
							</div>
							<div class="form-row">
								<div class="form-group col-md-6">
								<label for="inputCity" class="bmd-label-floating">City</label>
								<input type="text" class="form-control" id="inputCity">
								</div>
								<div class="form-group col-md-4">
								<label for="inputState" class="bmd-label-floating">State</label>
								<select id="inputState" class="form-control mb-3 custom-select">
								    <option >select State</option>
									<option>Gujarat</option>
									<option>Kashmir</option>
									<option>Himachal</option>
									<option>Panjab</option>
								</select>
								</div>
								<div class="form-group col-md-2">
								<label for="inputZip" class="bmd-label-floating">Zip</label>
								<input type="text" class="form-control" id="inputZip">
								</div>
							</div>	
                            <div class="form-row">
								<div class="form-group col-md-6">
								<label for="inputEmail4" class='m-b-30'>detail</label><br/>
								<textarea  class="form-control summernote " maxlength="225" rows="3" placeholder=""></textarea>
								</div>
								
							</div>							
							<button type="submit" class="btn btn-raised btn-primary mb-0">Sign in</button>
						</form>
					</div>
				</div>
			</div> <!-- end col -->
		</div> <!-- end row -->
								
	</div>
</div>
<?php
include_once'footer.php';
?>
                                       