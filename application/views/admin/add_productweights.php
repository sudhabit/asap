<?php
include_once'header.php';
?>
<div class="page-content-wrapper ">
    <div class="container-fluid">
        <div class='row'>  
            <div class="col-md-12 col-xl-12">
                <div class="card m-b-30 m-t-30">
                    <div class="card-body">
                        <h4 class="mt-0 header-title">Add Product Weights</h4>                 
                        <form action= "<?php echo base_url() ?>addProductweights" method="POST" enctype="multipart/form-data" class="mb-0">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                <label for="masterCategory" class="bmd-label-floating">Master Category</label>
                                    <select id="masterCategory" class="form-control mb-3 custom-select" name="master_category_id" required>
                                        <option>Select Master Category</option>
                                        <?php                    
                                            $count = count(array_filter($resultCnt));
                                            if($count > 0) {
                                            $i=0;
                                            foreach($resultCnt as $key => $row){
                                            ?>
                                                <option value="<?php echo  $row['id'] ?>"><?php echo  $row['master_category_name'] ?></option>
                                            <?php
                                            }
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                <label for="Category" class="bmd-label-floating">Category</label>
                                    <select id="catId" class="form-control mb-3 custom-select" name="category_id" required>  
                                      <option>Select  Category</option>
                                   </select>  
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                <label for="Subcategory" class="bmd-label-floating">Subcategory</label>
                                     <select id="subCatId" class="form-control mb-3 custom-select" name="subcategory_id" required>  
                                      <option>Select Subcategory</option>
                                   </select>
                                </div>
                                <div class="form-group col-md-6">
                                <label for="Productweight" class="bmd-label-floating">Product Weight</label>
                                    <input type="text" class="form-control" name="product_weight" required>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-raised btn-primary mb-0">Submit</button>
                        </form>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->
                                
    </div>
</div>
<?php
include_once'footer.php';
?>
                                       