<?php
include_once'header.php';
?>
<div class="page-content-wrapper ">
	<div class="container-fluid">
		<div class='row'>  
			<div class="col-md-12 col-xl-12">
				<div class="card m-b-30 m-t-30">
					<div class="card-body">
						<h4 class="mt-0 header-title">Edit Country</h4>					
						<form action= "<?php echo base_url() ?>Country/updatecountry" method="POST" enctype="multipart/form-data" class="mb-0">
							<div class="form-row">
								<div class="form-group col-md-6">
								<label for="inputCountrycode" class="bmd-label-floating">Country Code</label>
								<input type="text" class="form-control" name="country_code" value="<?php echo $result['country_code']; ?>">
								</div>
								<div class="form-group col-md-6">
								<label for="inputCountry" class="bmd-label-floating">Country</label>
								<input type="text" class="form-control" name="country_name" value="<?php echo $result['country_name']; ?>" required >
								</div>
							</div>
                            <input type="hidden" value="<?php echo $result['id']; ?>" name="id">
							<button type="submit" class="btn btn-raised btn-primary mb-0">Submit</button>
						</form>
					</div>
				</div>
			</div> <!-- end col -->
		</div> <!-- end row -->
								
	</div>
</div>
<?php
include_once'footer.php';
?>
                                       