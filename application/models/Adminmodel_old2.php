<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Adminmodel extends CI_Model{
    function __construct() {
        $this->userTbl = 'users';
        $this->empTable='employee';
    }
    //admin login
    
    function login($where,$table){
        $this->db->where($where); // $where is an array of condtions .
        $query =$this->db->get($table);
        $rows=$query->num_rows();
        if($rows>0)
        {
            
            $result = $query->result_array() ;
            return  $result ;  
           
        }   
        else
        {
            return false;
        }
        
    }
    function userLogin($where,$mobile,$columnList){
        $this->db->where($where); // $where is an array of condtions .
        $query =$this->db->get('users');
        $rows=$query->num_rows();
        if($rows>0)
        {
          $result = self::getUserUsingMobile($mobile,$columnList);
           return $result;
        }   
        else
        {
            return false;
        }
        
    }
    //get all records from user table
    function getAllrecords(){
        $count = $this->db->count_all_results('user');
        if($count > 0) {
            $query  = $this->db->get("users"); 
            $result = $query->result();
            return $result ;
        }
        else{
           return '0' ;  
        }

    }
    // for user register
    public function regUser($data){
        $result = $this->db->insert('users',$data);
        if($result){
            $id = $this->db->insert_id();
            return $id ;
        }
        else{
            return false;
        }

    }
    public function getUserUsingMobile($mob,$columnList){
        $this->db->where('user_mobile',$mob);
        $this->db->from('users');
        $count = $this->db->count_all_results();
        if($count>0){
            $this->db->select($columnList);
            $this->db->where('user_mobile',$mob);
            $query  = $this->db->get("users");
            $result = $query->result();
            return $result ;
        }
        else{
            return 0;
        }
    }
   
    public function queInsert($table,$data){
        $result = $this->db->insert($table,$data);
        if($result){
            return true ;
        }
        else{
            return false ;
        }
    }
    // for services
  
    public function userExist($where){
        $this->db->where($where);
        $userExist=$this->db->count_all_results('users');
        if($userExist > 0){
            return '1' ;
        }
        else{
            return '0';
        }  	 
    }
    // forget password
    public function forgetPwd($email){
        $this->db->where('email',$email);
		$userExist=$this->db->count_all_results('user');
        if($userExist > 0){
            $this->db->select('id,username,email,password');
            $this->db->where('email',$email);
            $query = $this->db->get('user');
            $result = $query->row();
			return $result ;
		}
		else{
			return false ;
		}
    }
    
    
     public function resetPassword($mobile,$data1){
        $this->db->where('user_mobile',$mobile);
        $query = $this->db->update('users',$data1);
        if($query){
            $result = self::getUserUsingMobile($mobile,$columnList);
            return $result;
        }
        else{
            return 0;
        }
    }
    public function checkPassword($data,$data1){
    $this->db->where($data);
    $result=$this->db->count_all_results('users');
    if($result > 0){
        $this->db->where($data);
        $this->db->update('users',$data1);
        return $result ;
    } 
    else
     return false;   
    }
    public function userOtp($where,$data1,$data2){
        $this->db->where($where) ;
        $result=$this->db->count_all_results('users_otp');
        if($result > 0){
        $this->db->where($where) ;
        $this->db->update('users_otp',$data1);
        return true ;
        } 
        else{
            
            $result = $this->db->insert('users_otp',$data2);
            if($result){
                return true ;
            }
            else{
                return false;
            }
        }
    }
    public function userVerifyOtp($otpCheck){
        $this->db->where($otpCheck);
        $result=$this->db->count_all_results('users_otp');      
        if($result > 0){
                return true ;
            }
            else{
                return false;
            }

    }
    // for banners 

    function getBanners($banner_id=null){
        if($banner_id==null || $banner_id=='') {
        
            $this->db->where('lkp_status_id',0);
            $count = $this->db->count_all_results('banners');
            if($count > 0) {
                 $this->db->where('lkp_status_id',0);
                $this->db->select('id,title,image');
                $query = $this->db->get('banners');
               $result = $query->result_array();
                return $result ;
            }
            else{
            return 0 ;  
            }
        }
        else
        {
             $this->db->where('lkp_status_id',0);
            $this->db->where('id',$banner_id);
            $this->db->from('banners');
            $count = $this->db->count_all_results();
            if($count>0){
                $this->db->select('id,title,image');
                $this->db->where('id',$banner_id);
                $query = $this->db->get('banners');
                $result = $query->result_array();
                return $result ;
            }
            else{
                return 0;
            }
        }

    }


    // for categories 

    function getcategory($cat_id=null){
        if($cat_id==null || $cat_id=='') {
        
             $this->db->where('lkp_status_id',0);
            $count = $this->db->count_all_results('categories');
            if($count > 0) {
                $this->db->where('lkp_status_id',0);
                $this->db->select('id,category_name,category_image');
                $query = $this->db->get('categories');
               $result = $query->result_array();
                return $result ;
            }
            else{
            return 0 ;  
            }
        }
        else
        {
            $this->db->where('lkp_status_id',0);
            $this->db->where('id',$cat_id);
            $this->db->from('categories');
            $count = $this->db->count_all_results();
            if($count>0){
                $this->db->select('id,category_name,category_image');
                $this->db->where('id',$cat_id);
                $query = $this->db->get('categories');
                $result = $query->result_array();
                return $result ;
            }
            else{
                return 0;
            }
        }

    }
    
    // for subcategory
    function getSubcategory($catid){
            $this->db->where('lkp_status_id',0);
            $this->db->where('category_id',$catid);
            $this->db->from('sub_categories');
            $count = $this->db->count_all_results();
            if($count>0){
                $this->db->where('lkp_status_id',0);
                $this->db->select('id,sub_category_name,image,category_id');
                $this->db->where('category_id',$catid);
                $query = $this->db->get('sub_categories');
                $result = $query->result_array();
                return $result ;
            }
            else{
                return 0;
            }
        

    }
    
     // for pages  

    function getCms($cms_id=null){
        if($cms_id==null || $cms_id=='') {
        
             $this->db->where('lkp_status_id',0);
            $count = $this->db->count_all_results('content_pages');
            if($count > 0) {
                $this->db->where('lkp_status_id',0);
                $this->db->select('id,title,image,description');
                $query = $this->db->get('content_pages');
               $result = $query->result_array();
                return $result ;
            }
            else{
            return 0 ;  
            }
        }
        else
        {
            $this->db->where('lkp_status_id',0);
            $this->db->where('id',$cms_id);
            $this->db->from('content_pages');
            $count = $this->db->count_all_results();
            if($count>0){
                $this->db->where('lkp_status_id',0);
                $this->db->select('id,title,image,description');
                $this->db->where('id',$cms_id);
                $query = $this->db->get('content_pages');
                $result = $query->result_array();
                return $result ;
            }
            else{
                return 0;
            }
        }

    }

         // for products

    function getProducts($id=null,$catid=null,$subcatid=null,$product_tag=null){
        if(($id==null || $id=='') && ($catid==null || $catid=='') && ($subcatid==null || $subcatid=='') && ($product_tag==null || $product_tag=='') ) {
        
             $this->db->where('lkp_status_id',0);
            $count = $this->db->count_all_results('products');
            if($count > 0) {
                $this->db->where('lkp_status_id',0);
                $this->db->select('id,product_name_english,product_name_telugu,product_name_hindi,category_id,sub_category_id, product_description,product_tags ');
                $query = $this->db->get('products');
               $result = $query->result_array();
                return $result ;
            }
            else{
            return 0 ;  
            }
        }
        
        elseif($catid!="")
        {
            $this->db->where('lkp_status_id',0);
            $this->db->where('category_id',$catid);
            $this->db->from('products');
            $count = $this->db->count_all_results();
            if($count>0){
                $this->db->where('lkp_status_id',0);
                $this->db->select('id,product_name_english,product_name_telugu,product_name_hindi,category_id,sub_category_id, product_description,product_tags ');
                $this->db->where('category_id',$catid);
                $query = $this->db->get('products');
                $result = $query->result_array();
                return $result ;
            }
            else{
                return 0;
            }
        }
        elseif($subcatid!="")
        {
            $this->db->where('lkp_status_id',0);
            $this->db->where('sub_category_id',$subcatid);
            $this->db->from('products');
            $count = $this->db->count_all_results();
            if($count>0){
                $this->db->where('lkp_status_id',0);
                $this->db->select('id,product_name_english,product_name_telugu,product_name_hindi,category_id,sub_category_id, product_description,product_tags ');
                $this->db->where('sub_category_id',$subcatid);
                $query = $this->db->get('products');
                $result = $query->result_array();
                return $result ;
            }
            else{
                return 0;
            }
        }
        elseif($product_tag!="")
        {
            if(!empty($product_tag)) {
        
             $this->db->where('lkp_status_id',0);
             $this->db->like('product_tags',$product_tag);
            $count = $this->db->count_all_results('products');
            if($count > 0) {
                $this->db->where('lkp_status_id',0);
                 $this->db->like('product_tags',$product_tag);
                $this->db->select('id,product_name_english,product_name_telugu,product_name_hindi,category_id,sub_category_id, product_description,product_tags');
                $query = $this->db->get('products');
               $result = $query->result_array();
                return $result ;
            }
            else{
            return 0 ;  
            }
        }
        }
        else
        {
            $this->db->where('lkp_status_id',0);
            $this->db->where('id',$id);
            $this->db->from('products');
            $count = $this->db->count_all_results();
            if($count>0){
                $this->db->where('lkp_status_id',0);
                $this->db->select('id,product_name_english,product_name_telugu,product_name_hindi,category_id,sub_category_id, product_description ');
                $this->db->where('id',$id);
                $query = $this->db->get('products');
                $result = $query->result_array();
                return $result ;
            }
            else{
                return 0;
            }
        }
        

    }

    // product search 
    function getProductsSearch($product_tag){
        if(!empty($product_tag)) {
        
             $this->db->where('lkp_status_id',0);
             $this->db->like('product_tags',$product_tag);
            $count = $this->db->count_all_results('products');
            if($count > 0) {
                $this->db->where('lkp_status_id',0);
                 $this->db->like('product_tags',$product_tag);
                $this->db->select('id,product_name_english,product_name_telugu,product_name_hindi,category_id,sub_category_id, product_description,product_tags ');
                $query = $this->db->get('products');
               $result = $query->result_array();
                return $result ;
            }
            else{
            return 0 ;  
            }
        }
        
        
       
        
        
        

    }

    // for feedback insert
        function feedback($data){
            $result = $this->db->insert('feedbacks',$data);
            if($result){
                $id = $this->db->insert_id();
                return $id ;
            }
            else{
                return false;
            }

        }
    // for state getState
    function getState(){
            $this->db->where('lkp_status_id',0);
            $this->db->from('lkp_states');
            $count = $this->db->count_all_results();
            if($count>0){
                $this->db->where('lkp_status_id',0);
                $query = $this->db->get('lkp_states');
                $result = $query->result_array();
                return $result ;
            }
            else{
                return 0;
            }
    }
    
    //getDistricts
    
    function getDistricts($id){
        $this->db->where('lkp_status_id',0);
        $this->db->where('lkp_state_id',$id);
        $this->db->from('lkp_districts');
        $count = $this->db->count_all_results();
        if($count>0){
            $this->db->where('lkp_status_id',0);
            $this->db->where('lkp_state_id',$id);
            $query = $this->db->get('lkp_districts');
            $result = $query->result_array();
            return $result ;
        }
        else{
            return 0;
        }
    }
   // get getPincode
   function getPincode($id){
        $this->db->where('lkp_status_id',0);
        $this->db->where('lkp_district_id',$id);
        $this->db->from('lkp_pincodes');
        $count = $this->db->count_all_results();
        if($count>0){
            $this->db->where('lkp_status_id',0);
            $this->db->where('lkp_district_id',$id);
            $query = $this->db->get('lkp_pincodes');
            $result = $query->result_array();
            return $result ;
        }
        else{
            return 0;
        }
    }
    //getMandala 
    
    function getMandala($id){
        $this->db->where('lkp_status_id',0);
        $this->db->where('lkp_pincode_id',$id);
        $this->db->from('lkp_cities');
        $count = $this->db->count_all_results();
        if($count>0){
            $this->db->where('lkp_status_id',0);
            $this->db->where('lkp_pincode_id',$id);
            $query = $this->db->get('lkp_cities');
            $result = $query->result_array();
            return $result ;
        }
        else{
            return 0;
        }
    }
    
    function getVillage($id){
        $this->db->where('lkp_status_id',0);
        $this->db->where('lkp_city_id',$id);
        $this->db->from('lkp_areas');
        $count = $this->db->count_all_results();
        if($count>0){
            $this->db->where('lkp_status_id',0);
            $this->db->where('lkp_city_id',$id);
            $query = $this->db->get('lkp_areas');
            $result = $query->result_array();
            return $result ;
        }
        else{
            return 0;
        }
    }
    
    function getOrderDetail($userId){
        
        $this->db->where('status',0);
        $this->db->where('user_id',$userId);
        $this->db->from('orders');
        $count = $this->db->count_all_results();
        if($count>0){
            $this->db->where('status',0);
            $this->db->where('user_id',$userId);
            $query = $this->db->get('orders');
            $result = $query->result_array();
            return $result ;
        }
        
    }
    function getSingleOrderDetail($userId,$orderId){
        $this->db->where('status',0);
        $this->db->where('user_id',$userId);
        $this->db->where('order_id',$orderId);
        $this->db->from('orders');
        $count = $this->db->count_all_results();
        if($count>0){
            $this->db->where('status',0);
            $this->db->where('user_id',$userId);
            $this->db->where('order_id',$orderId);
            $query = $this->db->get('orders');
            $result = $query->result_array();
            return $result ;
        }
    }
    function categoryList($columnList){       
        $this->db->where('isactive',0);
        $this->db->from('category');
        $count = $this->db->count_all_results();
        if($count>0){
            if($columnList !=""){
                $this->db->select($columnList);
            }
            $this->db->where('isactive',0);      
            $query = $this->db->get('category');
            $result = $query->result_array();
            return $result ;
        }
    }
    //restaurantList
    function restaurantList($lat,$long,$checkPoint,$minLimit,$maxLimit,$distance){
        //"CALL uspGetAllCategories(1)        
           $query=$this->db->query("CALL vendorsList($lat,$long,$checkPoint,$minLimit,$maxLimit,$distance)");
           mysqli_next_result( $this->db->conn_id );
           $num =$query->num_rows();
           if($num > 0){
                $result = $query->result_array();
                //$query->next_result();
               // $query->free_result();
            return $result ;
           }else{
               return 0;
           }
          
    }
   


    function restaurantBranchList($lat,$long,$venid,$distance,$grp){  
            mysqli_next_result($this->db->conn_id);
           $query2=$this->db->query("CALL getBrancheList($lat,$long,$venid,$distance,$grp)");
          //  $this->db->group_by('vendorId');
           $num =$query2->num_rows();
           if($num > 0){                
            $result2 = $query2->result_array();
            mysqli_next_result($this->db->conn_id);
            return $result2[0];
           }else{
               return 0;
           }
    }
    //restaurantBranchList
     function restaurantItemList($vendorId,$branchId,$catId=null,$dishType=null,$search=null){
        $this->db->select('tblvendorproducts.id,tblvendorproducts.productName,tblvendorproducts.productDescription,tblvendorproducts.categoryId,tblvendorproducts.subCategoryId,tblvendorproducts.productType,tblvendorproducts.productImage,tblproductprices.id,tblproductprices.vendorId,tblproductprices.branchId,tblproductprices.productId,tblproductprices.productWeight,tblproductprices.productPrice');
        $this->db->from('tblvendorproducts');
        $this->db->join('tblproductprices', 'tblproductprices.productId = tblvendorproducts.id', 'left');
        $this->db->where('tblproductprices.lookupStatusId','0');
        $this->db->where('tblvendorproducts.lookupStatusId','0');
        
        $this->db->where('tblproductprices.vendorId', $vendorId);
        $this->db->where('tblproductprices.branchId', $branchId);
        
        if($catId !=""){
           // $this->db->where('tblvendorproducts.categoryId', $catId);
        }
        if($dishType !=""){
            $this->db->where('tblvendorproducts.productType', $dishType);
        }
        if($search !=""){
            $this->db->like('tblvendorproducts.productName', $search);
        }
        $query = $this->db->get();  
        $num =$query->num_rows();
           if($num > 0){                
            $result2 = $query->result_array();
            mysqli_next_result($this->db->conn_id);
            return $result2 ;
           }else{
               return 0;
           }
    }
    
    public function productAddon($branchId,$productId){
        $this->db->select('apa.id,apa.branchId,apa.productId,apa.addonId,pa.addonName,pap.addonPrice');
        $this->db->from('tbl_assign_product_addons apa');
        $this->db->join('tbl_product_addon pa', 'pa.id=apa.addonId');
        $this->db->join('tbl_product_addon_price pap','pap.addonId=apa.addonId');    
        $this->db->where('apa.lookupStatusId','0');
        $this->db->where('pa.lookupStatusId','0');        
        $this->db->where('apa.branchId', $branchId);
        $this->db->where('apa.productId', $productId);
        $query = $this->db->get();  
        $num =$query->num_rows();
           if($num > 0){                
            $result2 = $query->result_array();
            mysqli_next_result($this->db->conn_id);
            return $result2 ;
           }else{
               return 0;
           }
    }
    
    // new methods  by sudha
	
	public function checkUserAuthKey($userId,$user_token){
        $data = array("user_code"=>"$userId","user_token"=>"$user_token");
        $this->db->where($data);
        $result=$this->db->count_all_results('users');
        if($result > 0){

            return 1;
        }
        else{
            return 0;
        }
    }
	public function getUsers($user_code,$user_token){		
		$data = array("user_code"=>$user_code,"user_token"=>$user_token);
		$this->db->select('id,user_code,user_name,user_email,user_mobile,user_token,user_password');
        $this->db->where($data);
	    $this->db->from('users');
		$query = $this->db->get();  
        $num =$query->num_rows();       
		if($num > 0){                
			$result = $query->result_array();
			
			return $result;
        }
        else{
            return false;
        }
		
	}
	
	 public function updateUsers($data,$where){
		$this->db->where($where);
	    $result=$this->db->count_all_results('users');
		if($result > 0){
			$this->db->where($where);
			$this->db->update('users',$data);
			return $result ;
		} 
		else
		 return false;   
    }
    //For Master Categories -- HR
   public function getMasterCategory($table){
       //$id =$this->input->post('id');
       $result1=[];
       $this->db->where('isactive','0');
       $query = $this->db->get($table);
       $result = $query->result_array();
       if($result){
           $result1 =$result;
           return $result1 ;
       }
       else{
           return $result1 ;
       }
   }
   //To Insert Data -- HR
   public function insertRecordQueryList($table,$data){
       $result = $this->db->insert($table,$data);
       if($result){
           return true ;
       }
       else{
           return false;
       }
   }
   //To Check Data whether exists or not -- HR
   public function existData($where,$tbale){

       $this->db->where($where);
       $userExist=$this->db->count_all_results($tbale);
       if($userExist > 0){
           return 1 ;
       }
       else{
           return 0;
       }
   }
   //To Display Data -- HR
   public function singleRecordData($column,$value,$table){
       $this->db->where($column,$value);
       $this->db->from($table);
       $count = $this->db->count_all_results();
       if($count>0){
           $this->db->where($column,$value);
           $query = $this->db->get($table);
           $result = $query->result_array();
           return $result ;
       }
       else{
           return 0;
       }
   }
   //To Display Single Column Name -- HR
   public function getSingleColumnName($value,$column,$expColumn,$table){
       $this->db->where($column,$value);
       $query = $this->db->get($table);
       $result = $query->row();
       if($result){
           return $result->$expColumn ;
       }
       else{
           return "" ;
       }
   }
   //To Display all Records -- HR
   public function get_current_page_records($table,$limit, $start,$column=null,$value=null,$search=null,$searchColumn=null)
  {
      if($search!='null' && $search!='' ){
          $this->db->like($searchColumn,$search);
      }
      if($column!=null && $value!=null){
          $this->db->where($column,$value);
      }
      $this->db->limit($limit, $start);
      $query = $this->db->get($table);

      if ($query->num_rows() > 0)
      {
           $data = $query->result_array();
          return $data;
      }

      return false;
  }
  //To get Record count -- DV
   public function record_count($table,$search=null,$searchColumn) {
       if($search!='null'){
           return $this->db
           ->like($searchColumn,$search)
           ->count_all_results($table);
       }else{
           return $this->db->count_all($table);
       }
   }
   //Update records
   public function updateRecordQueryList($table,$data,$id,$value){
       $this->db->where($id,$value) ;
       $result=$this->db->count_all_results($table);
       if($result > 0){
       $this->db->where($id,$value) ;
       $this->db->update($table,$data);
           return true ;
       }
       else{
           return false;
       }
   }
    public function updateRecordQueryList2($table,$data,$where){        
        $this->db->where($where) ;
        $result=$this->db->count_all_results($table);
        if($result > 0){
        $this->db->where($where) ;
        $this->db->update($table,$data);
            return true ;
        } 
        else{
            return false;
        }
    }
   //To get Data -- DV
   public function getAjaxdata($column,$value,$table){
        $result1 =[];    
        //$id =$this->input->post('id');
        $this->db->where($column,$value);
        $query = $this->db->get($table);
        $result = $query->result_array();
        if($result){
            $result1 =$result;
            return $result1 ;
        } else{
            return $result1 ;
        }
    }
    //for delete multiple images
    public function delmultipleImage($code,$table,$folder,$folder1,$selectColumn,$selectColumn1,$selectColumn2){
        $this->db->select($selectColumn);
        $this->db->select($selectColumn1);
        $this->db->where($selectColumn2,$code);
        $query = $this->db->get($table);
        $row  = $query->row_array();
        $url= FCPATH;
        if($row[$selectColumn] != '' || $row[$selectColumn] != null){
            $file = $row[$selectColumn];
            unlink($url."uploads/".$folder."/".$file);
        }if($row[$selectColumn1] != '' || $row[$selectColumn1] != null){
            $file1 = $row[$selectColumn1];
                unlink($url."uploads/".$folder1."/".$file1);
        }
        $this->db->where($selectColumn2,$code);
        $result = $this->db->delete($table);
        if($result){
            return true ;
        }
        else{
            return false;
        }     
    }
    // for deleterow
    public function delRow($id,$table){
        $this->db->where('id',$id);
        $result = $this->db->delete($table);
        if($result){
            return true ;
        }
        else{
            return false;
        }
    } 
    //For where in condition
    public function whereIn($value,$column,$table) {
        $result1 =[];
        $this->db->where_in($value, $column);
        $query = $this->db->get($table);
        $result = $query->result_array();
        if($result){
            $result1 =$result;
            return $result1 ;
        } else{
            return $result1 ;
        }
    }
    //for signle column
    //getSingleColumnName($field3['per_country'],'id','country_name','keyaan_countries') ;
    function singleColumnData($column,$value,$expect_column,$table){
        mysqli_next_result($this->db->conn_id);
        $this->db->select($expect_column); 
        $this->db->where($column,$value);   
        $query =$this->db->get($table);       
        $rows=$query->num_rows();
        //echo $this->db->last_query(); die;
        if($rows > 0)
        {
            $result = $query->row();   
            return $result->$expect_column;
        }  
       else{
           return '';
       }
    }
   




    // subcat list 
    function restaurantSubCatList($vendorId,$branchId,$catId){ 
        $this->db->select('product_sub_cat_id');
        $this->db->where('product_cat_id',$catId);
        $this->db->where('branch_code',$branchId);
        $this->db->where('vendor_code',$vendorId);
        $this->db->group_by('product_sub_cat_id');
        $query = $this->db->get('product_weights_prices');
        $num =$query->num_rows();
        if($num > 0){                
            $result= $query->result_array();
            return $result;
        }else{
            $emptyArr = array();
            return $emptyArr;
        }
    }

     // branch category List sudha
    
     function restaurantCatList($vendorId,$branchId){ 
        $this->db->select('product_cat_id');
        $this->db->where('branch_code',$branchId);
        $this->db->where('vendor_code',$vendorId);
        $this->db->group_by('product_cat_id');
        $query = $this->db->get('product_weights_prices');
        $num =$query->num_rows();
        if($num > 0){                
            $result= $query->result_array();
            return $result;
        }else{
            $emptyArr = array();
            return $emptyArr;
        }
    }

    //restaurantproductList
    function restaurantproductList($vendorId,$branchId,$catId,$subcat=NULL){ 
        $this->db->select('product_id');
        $this->db->where('product_cat_id',$catId);
        $this->db->where('branch_code',$branchId);
        $this->db->where('vendor_code',$vendorId);
        IF($subcat!='null'){
            $this->db->where('product_sub_cat_id',$subcat);
        }
        $this->db->group_by('id');
        $query = $this->db->get('product_weights_prices');
        $num =$query->num_rows();
        if($num > 0){                
            $result= $query->result_array();
            return $result;
        }else{
            $emptyArr = array();
            return $emptyArr;
        }
    }

    //restaurantProductPriceList
    //restaurantproductList
    function restaurantProductPriceList($vendorId,$branchId,$pid){ 
        $this->db->select('product_weight_id,product_price');
        $this->db->where('branch_code',$branchId);
        $this->db->where('vendor_code',$vendorId);
        $this->db->where('product_id',$pid);
        $query = $this->db->get('product_weights_prices');
        $num =$query->num_rows();
        if($num > 0){                
            $result= $query->result_array();
            return $result;
        }else{
            $emptyArr = array();
            return $emptyArr;
        }
    }
    
    function restaurantProductRecomanded($vendorId,$branchId){ 
        $this->db->select('product_weight_id,product_price,product_cat_id,product_sub_cat_id,product_id');
        $this->db->where('branch_code',$branchId);
        $this->db->where('vendor_code',$vendorId);
        $query = $this->db->get('product_weights_prices');
        $num =$query->num_rows();
        if($num > 0){                
            $result= $query->result_array();
            return $result;
        }else{
            $emptyArr = array();
            return $emptyArr;
        }
    }

    function branchDetail($vendorId,$branchId){ 
        $this->db->select('vendor_code,branch_code,branch_adress,latitude,longitude,delivery_time,min_order,delivery_charges');
        $this->db->where('branch_code',$branchId);
        $this->db->where('vendor_code',$vendorId);
        $query = $this->db->get('vendor_branch_details');
        $num =$query->num_rows();
        if($num > 0){                
            $result= $query->result_array();
            return $result;
        }else{
            $emptyArr = array();
            return $emptyArr;
        }
    }

    function fetchAllrecords($table,$where,$columnList){
        $this->db->select($columnList);
        $this->db->where($where);
        $query = $this->db->get($table);
        $num =$query->num_rows();
        if($num > 0){                
            $result= $query->result_array();
            return $result;
        }else{
            $emptyArr = array();
            return $emptyArr;
        }

    }
    public function updateAsapRecord($table,$data,$where_data){
        $this->db->where($where_data);
        $query = $this->db->get($table);
        $num =$query->num_rows();
        if($num > 0){ 
        $this->db->where($where_data);    
        $result = $this->db->update($table,$data);
        //echo $this->db->last_query(); die;
            if($result){           
                return true;
            }
            else{
                return false;
            }
        }else{
            return false;
        }
    }
     //updateDefaultAddress
     public function updateDefaultAddress($table,$data,$where_data){
        $this->db->where($where_data);
        $query = $this->db->get($table);
        $num =$query->num_rows();
        if($num > 0){ 
        $this->db->where($where_data);    
        $result = $this->db->update($table,$data);
            if($result){   
                $data_def =array(
                    default_address => 0,
                );
                $this->db->where('id!=',$where_data['id']);    
                $result = $this->db->update($table,$data_def);     
                return true;
            }
            else{
                return false;
            }
        }else{
            return false;
        }
    }
    public function generateOrderId($newOrderId){
        $sql="SELECT order_id FROM order_details WHERE order_id  = '".$newOrderId."' "; 
        $query = $this->db->query($sql);
        $num =  $query->num_rows();
        if($num > 0)
        {   
            $row=$query->row();
            $order_id = $row->order_id+1;
            return $this->generateOrderId($order_id);
        }
        else {      
            return  $newOrderId;  
        } 

    }
    public function lastOrderId(){
        $sql="SELECT order_id FROM order_details  order by id desc limit 0,1 "; 
        $query = $this->db->query($sql);
        $num =  $query->num_rows();
        if($num > 0)
        {   
            $row=$query->row();
            return $row->order_id ;
        }
        else {    
            $rand = 1000  ;
            return  $rand;  
        } 

    }


}
?>