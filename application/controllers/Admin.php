<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper("encryptionpwd");
        $this->load->library('form_validation');
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
    }
    public function index() { 
        if(@$this->session->userdata(isUserLoggedIn)) {
           $this->load->view('admin/add_vendor');   
        }
        else {
            $this->load->view('admin/index');
        }        
    }
    public function adminLogin(){
        if(@$this->session->userdata(isUserLoggedIn)) {
           redirect('viewCategory');    
        }
        else {
            if($this->input->post('username')){                   
                $where= array(
                    'username'=>$this->input->post('username'),
                    'useremail' =>$this->input->post('password'),
                    'isactive' => '0'
                );                        
                $checkLogin = $this->Adminmodel->login($where,'master_login');
                if($checkLogin){
                    $this->session->set_userdata('isUserLoggedIn',TRUE);
                    $this->session->set_userdata('userId',$checkLogin[0]['username']);
                    $this->session->set_userdata('userCode',$checkLogin[0]['id']);                                      
                    redirect('viewCategory');
                } else{                            
                    $this->session->set_flashdata('error_mesg','<div class="alert alert-danger">Wrong email or password, please try again.</div>');
                }
            } 
            $this->load->view('admin/index');
        }
    }
    //Logout Functionality 
    public function logout(){
        $this->session->unset_userdata('isUserLoggedIn');
        $this->session->unset_userdata('userId');
            $this->session->unset_userdata('id');
        redirect("admin");
    }  
    
    private function set_upload_options() {   
        //upload an image options
        $config = array();
        $config['upload_path'] = base_url()."uploads/vendorimage/";
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size']      = '0';
        $config['overwrite']     = FALSE;
        return $config;
    } 
    public function pincode(){
       $this->load->view('admin/pincodeAjax');
    }

}

