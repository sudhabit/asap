<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Productaddon extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
        $this->load->helper("encryptionpwd");
        $this->load->library('form_validation');
    }
     public function index() {
        self::viewProductaddon();
       } 
    	public function addProductaddon(){
        if(!is_vendorlogged_in())  // if you add in constructor no need write each function in above controller.
         {
         redirect('Mastervendor');
         }
	      	$addon_name = $this->input->post('addon_name'); 
                $vendor = $this->session->userdata('vendorCode');      
	        if($addon_name!=''){            
	        $check_data = array(
                    "vendor_code" => $vendor,
	            "addon_name" => $this->input->post('addon_name')    
	        );
	        $tablename = "product_addon";
	        $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
	        if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger">Product addon already exist</div>') ;
                $url='addProductaddon';
                redirect($url);
	        }else{ 
                $added_by = $vendor!='' ? $vendor:'vendor' ; 
	              $addon_description = $this->input->post('addon_description')=="" ? "":$this->input->post('addon_description');
	              $date     = date("Y-m-d H:i:s");
	              $data = array(
	                  'addon_description'=> $addon_description ,
	                  'addon_name'=> $addon_name ,
                    'vendor_code'=>   $vendor,
	                  'created_by'     => $added_by ,
	                  'created_at'     => $date,
	                  'updated_at'     => $date,
	                  'updated_by'     => $added_by
	              );
	                $table="product_addon";
	                $result = $this->Adminmodel->insertRecordQueryList($table,$data);
	                if($result){
	                   $this->session->set_flashdata('msg','<div class="alert alert-success updateSuss">Product addon Added Successfully!</div>');
	                }
	                else{
	                   $this->session->set_flashdata('msg','<div class="alert alert-danger">opp! Data not inserted</div>') ;
	                }           
	                $url='viewProductaddon';
	                redirect($url);
	            }
	            }else
	            {
	                //$this->session->set_flashdata('msg','<div class="alert alert-danger">Category name should not be blanck</div>') ;
	                $this->load->view('vendor/add_productaddon');    
	            }     
	        }
        public function viewProductaddon(){
          if(!is_vendorlogged_in())  // if you add in constructor no need write each function in above controller.
           {
           redirect('Mastervendor');
           }
           $table ="product_addon";
           $search = ($this->input->get("search"))? $this->input->get("search") : "null";
           $config = array();
           $config['reuse_query_string'] = true;
           $config["base_url"] = base_url() . "Productaddon/viewProductaddon";
           $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
           $config["total_rows"] = $this->Adminmodel->record_count($table,$search,'addon_name');//search
           $config["per_page"] = PERPAGE_LIMIT;
           $config["uri_segment"] = 3;
           $config['full_tag_open'] = "<ul class='pagination'>";
           $config['full_tag_close'] = '</ul>';
           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';
           $config['cur_tag_open'] = '<li class="active"><a href="#">';
           $config['cur_tag_close'] = '</a></li>';
           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';
           $config['first_tag_open'] = '<li>';
           $config['first_tag_close'] = '</li>';
           $config['last_tag_open'] = '<li>';
           $config['last_tag_close'] = '</li>';
           $config['prev_link'] = '<i class="mdi mdi-skip-backward"></i>';
           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';
           $config['next_link'] = '<i class="mdi mdi-skip-forward"></i>';
           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';
           $this->pagination->initialize($config);
           $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
           $data["links"] = $this->pagination->create_links();
           $limit =$config["per_page"];
           $start=$page;
           $vendor = $this->session->userdata('vendorCode');
           $result = $this->Adminmodel->get_current_page_records($table,$limit,$start,'vendor_code',$vendor,$search,'addon_name');
              if($result){
                $data['result'] = $result;
              } else {
                $result[] = [] ;
                $data['result'] = $result;
              }
              $data['searchVal'] = $search !='null'?$search:"";
              $this->load->view('vendor/view_productaddon',$data);
            }
      public function editProductaddon(){
         if(!is_vendorlogged_in())  // if you add in constructor no need write each function in above controller.
           {
           redirect('Mastervendor');
           }
          $id = $this->uri->segment('3');
          if($id==''){
              redirect('vendorLogin');
          }
          $tablename = "product_addon";
          $result = $this->Adminmodel->singleRecordData('id',$id,$tablename);
          $data['result'] = $result[0] ;
          if($result) {
             $this->load->view('vendor/edit_productaddon',$data);
         } else {
             $url='viewProductaddon';
             redirect($url);
         }

              
      }
      public function updateProductaddon(){
       if(!is_vendorlogged_in())  // if you add in constructor no need write each function in above controller.
         {
         redirect('Mastervendor');
         }
        $id = $this->input->post('id');
        if(empty($id)){
            redirect('vendorLogin');
        }
        $addon_name = $this->input->post('addon_name');   
        $vendor = $this->session->userdata('vendorCode');    
        if($addon_name!=''){            
            $check_data = array(
                "vendor_code" => $vendor,
                "addon_name" => $addon_name,
                "id !=" =>$id   
            );
            $tablename = "product_addon";
            $checkData = $this->Adminmodel->existData($check_data,$tablename) ;
            if($checkData > 0){
                $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Product Addon already exist</div>') ;
            }else{
                $added_by = $vendor!='' ? $vendor:'vendor' ;
                $addon_description = $this->input->post('addon_description')=="" ? "":$this->input->post('addon_description');           
                $date     = date("Y-m-d H:i:s");
                $id =$this->input->post('id');
                $data = array(
                    'addon_name'=> $addon_name,
                    'addon_description'=> $addon_description ,
                    'vendor_code'=>   $vendor,
                    'updated_at'     => $date,
                    'updated_by'     => $added_by
                );
                $table="product_addon";
                $result = $this->Adminmodel->updateRecordQueryList($table,$data,'id',$id);
                if($result){
                    $this->session->set_flashdata('msg','<div  class="alert alert-success updateSuss">Product Addon Updated.</div>');
                }else{
                    $url='Productaddon/editProductaddon/'.$id;
                    redirect($url);
                    $this->session->set_flashdata('msg','<div class="alert alert-danger updateSuss">Opps! Some error, Product Addon not updated.</div>') ;
                }   
            } 
            $url='viewProductaddon';
            redirect($url);
        }else {   
            $url='Productaddon/editProductaddon/'.$id;
            redirect($url); 
        }
        
}
             
    	
         function productaddonEnable($id)
        {
            $id=$id;
            $dataSubcat =array(
                'isactive' =>'0'
            );
            $table="product_addon";
            $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
            $url='Productaddon/viewProductaddon';
            redirect($url);
        }
        function productaddonDisable($id)
        {
            $id=$id;
            $dataSubcat =array(
                'isactive' =>'1'
            );
            $table="product_addon";
            $result = $this->Adminmodel->updateRecordQueryList($table,$dataSubcat,'id',$id);
            $url='Productaddon/viewProductaddon';
            redirect($url);
        }
        function deleteproductaddon($id) {
        $id=$id;
        $result = $this->Adminmodel->delRow($id,'product_addon');
        $data['result'] =$result;
        redirect($_SERVER['HTTP_REFERER']);
    }

}
?>
