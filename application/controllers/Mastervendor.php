<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mastervendor extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper("encryptionpwd");
        $this->load->library('form_validation');
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
    }
    public function index() { 
        if(@$this->session->userdata(isVendorLoggedIn)) {
            redirect('Vendordetails');
        }
        else {
            $this->load->view('vendor/index');
        }        
    }
    public function vendorLogin(){
        if(@$this->session->userdata(isVendorLoggedIn)) {
           redirect('Vendordetails');    
        }
        else {
            if($this->input->post('username')){                   
                $where= array(
                    'username'=>$this->input->post('username'),
                    'password' =>$this->input->post('password'),
                    'isactive' => '0'
                );                        
                $checkLogin = $this->Adminmodel->login($where,'vendor_login');
                if($checkLogin){
                    $this->session->set_userdata('isVendorLoggedIn',TRUE);
                    $this->session->set_userdata('vendorId',$checkLogin[0]['username']);
                    $this->session->set_userdata('vendorCode',$checkLogin[0]['vendor_code']);
                    redirect('Vendordetails');
                } else{                            
                    $this->session->set_flashdata('error_mesg','<div class="alert alert-danger">Wrong email or password, please try again.</div>');
                }
            } 
            $this->load->view('vendor/index');
        }
    }
    //Logout Functionality 
    public function logout(){
        $this->session->unset_userdata('isVendorLoggedIn');
        $this->session->unset_userdata('vendorId');
        $this->session->unset_userdata('vendorCode');
        redirect("Mastervendor");
    }

}

