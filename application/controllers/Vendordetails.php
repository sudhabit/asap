<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendordetails extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper("encryptionpwd");
        $this->load->library('form_validation');
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
    }
    public function Vendordetails(){
        if(!is_vendorlogged_in())  // if you add in constructor no need write each function in above controller. 
        {
            redirect('Mastervendor');
        }
        $start=0;
        $perPage = 100;
        $vendor = $this->session->userdata('vendorCode');
        $vendorId=$this->Adminmodel->getSingleColumnName($vendor,'vendor_code','id','vendor');
        $table="vendor";
        if($vendorId !=""){
            @$column = "id";
            @$value  = $vendorId;
        }
        $search ='';

        $result1 = $this->Adminmodel->get_current_page_records($table,$perPage,$start,@$column,@$value,@$search,@$searchColumn);
        $result=replace_attr($result1);
        if($result){
        foreach ($result as $key => $field) {
            $data['vendor_pan_image'] = base_url()."uploads/vendor_pan_image/".$field['vendor_pan_image'];
            $data['vendor_adhar_image'] = base_url()."uploads/vendor_adhar_image/".$field['vendor_adhar_image'];
            $data['business_pan_mage'] = base_url()."uploads/business_pan_mage/".$field['business_pan_mage'];
            $data['registration_certificate_image'] = base_url()."uploads/registration_certificate_image/".$field['registration_certificate_image'];
            $data['web_logo'] = base_url()."uploads/web_logo/".$field['web_logo'];
            $data['app_logo'] = base_url()."uploads/app_logo/".$field['app_logo'];
            $data['web_restaurant_image'] = base_url()."uploads/web_restaurant_image/".$field['web_restaurant_image'];
            $data['app_restaurant_image'] = base_url()."uploads/app_restaurant_image/".$field['app_restaurant_image'];
            $catList = $this->Adminmodel->singleRecordData('vendor_code',$field['vendor_code'],'vendor_service_categories');
            if($catList) {
                foreach ($catList as $key => $field) {
                    $catList[$key]['category_name'] = $this->Adminmodel->getSingleColumnName($field['category_id'],'id','category_name','category') ;

                }
            }
            $cusineList = $this->Adminmodel->singleRecordData('vendor_code',$field['vendor_code'],'vendor_service_cusinetypes');
            if($cusineList) {
                foreach ($cusineList as $key => $field) {
                    $cusineList[$key]['cusine_type_name'] = $this->Adminmodel->getSingleColumnName($field['cusine_type_id'],'id','cusine_type_name','cusine_types') ;
                }
            }
        }
        $data['category'] = $catList ;
        $data['cusineType'] = $cusineList ;
        $data['result'] = $result[0] ;
        $this->load->view('vendor/vendor_Details',$data);
        }
        else{
            $url=base_url().'Mastervendor';
            redirect($url);
        }
   }
    
   
}
?>

