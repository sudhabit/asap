<?php
header('Access-Control-Allow-Origin: *');
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH . '/libraries/REST_Controller.php');

/**
 * Description of RestPostController
 * 
 */
class AsapService  extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->helper("encryptionpwd");
        $this->load->helper("sms");
        $this->load->model('Adminmodel');
    }
    public function registerUser_post(){
        $input         = json_decode(file_get_contents('php://input'), true); 
        if(!empty($input['mobile']) && !empty($input['device_type']) ){
                         $mobile        = $input['mobile'];
			$email        =  $input['email'];
			$name        =   $input['name'];			
			$regType = $input['device_type'];
            $mob="/^[1-9][0-9]*$/";
            if(!preg_match($mob,$input['mobile']) || strlen($input['mobile'])!='10'){
                $responseList = self::success_response('FALSE','6',MESSAGE_INVALDMOBILE);
                $this->set_response($responseList,REST_Controller::HTTP_OK);
            }elseif($regType !='2' && $regType !='3'){
                $responseList = self::success_response('FALSE','11',MESSAGE_INVALDEMAIL);
                $this->set_response($responseList,REST_Controller::HTTP_OK);
            }
            else{
                $userCheck = array(
                'user_mobile' => $mobile
           	 );
                $checkData = $this->Adminmodel->userExist($userCheck) ;
                if($checkData > 0){
                    $responseList = self::success_response('FALSE','4',MESSAGE_MOBILEEXIST);
                    $this->set_response($responseList,REST_Controller::HTTP_OK);
                }else{               
                    
                    $responseList = self::success_response('TRUE','0',MESSAGE_SUCCESSREG);
                    $this->set_response($responseList,REST_Controller::HTTP_OK); 
                }
            }
        }else{
            $responseList = self::success_response('FALSE','2',MESSAGE_PARAMETERMISSING);
            $this->set_response($responseList,REST_Controller::HTTP_OK);
        } 
    }
    /*
    Login servive 
    parameters--> username , password
    method-->post
    */
   public function loginUser_get(){       
        $mobile = $this->input->get('mobile', TRUE); 
        $password = $this->input->get('password', TRUE); 
        if(isset($mobile) && !empty($password) ){
            $mob="/^[1-9][0-9]*$/";
            if(!preg_match($mob,$mobile) || strlen($mobile)!='10') {
                $responseList = self::success_response('FALSE','6',MESSAGE_INVALDMOBILE);
                $this->set_response($responseList,REST_Controller::HTTP_OK);
            }else{
                $data = array(
                'user_mobile'   => $mobile,
                'user_password' => encryptPassword($password),
                );               
                $result = $this->Adminmodel->userLogin($data,$mobile,unserialize(USERCOLUMNLIST));
                if($result)
                {
                    $responseList = self::success_response('TRUE','0',MESSAGE_SUCCESS,$result[0]);
                    $this->set_response($responseList,REST_Controller::HTTP_OK); 
                }
                else
                {
                    $responseList = self::success_response('FALSE','1',MESSAGE_INVALIDCREDENTIALS);
                    $this->set_response($responseList,REST_Controller::HTTP_OK); 
                }  
            }
        }else{           
            $responseList = self::success_response('FALSE','2',MESSAGE_PARAMETERMISSING);
            $this->set_response($responseList,REST_Controller::HTTP_OK);
        }
    }
    public function fogetPassword_get(){
        $email = $this->input->get('email', TRUE); 
        $result = $this->Adminmodel->forgetPwd($email);
        if($result){          
            $responseList = self::success_response('TRUE','0',MESSAGE_SUCCESSPWD,$result[0]);
            $this->set_response($responseList,REST_Controller::HTTP_OK); 
        }
        else{
            $responseList = self::success_response('FALSE','1',MESSAGE_NORECORD);
            $this->set_response($responseList,REST_Controller::HTTP_OK); 
        }
    }
	//User profile 
	public function userProfile_get(){  
        $user_code = $this->input->get('userId', TRUE);
		$user_token = $this->input->get('user_token', TRUE);
		if(!empty($user_code) && !empty($user_token)){
			$checkToken = $this->Adminmodel->checkUserAuthKey($user_code,$user_token);
                       
			if($checkToken > 0){				
				$result = $this->Adminmodel->getUsers($user_code,$user_token);
                                
				if($result){
                    $responseList = self::success_response('TRUE','0',MESSAGE_SUCCESS,$result[0]);
                    $this->set_response($responseList,REST_Controller::HTTP_OK); 
				}
				else{
                    $responseList = self::success_response('FALSE','1',MESSAGE_NORECORD);
                    $this->set_response($responseList,REST_Controller::HTTP_OK);
				}
			}else{
                $responseList = self::success_response('FALSE','12',MESSAGE_INVALDTOKEN);
                $this->set_response($responseList,REST_Controller::HTTP_OK);
			}
		}else{
            $responseList = self::success_response('FALSE','2',MESSAGE_PARAMETERMISSING);
            $this->set_response($responseList,REST_Controller::HTTP_OK);
		}
    }
	//update  profile 
	public function updateProfile_put(){
        $input = json_decode(file_get_contents('php://input'), true);  
        $user_code = $input['userId'];
		$user_token = $input['user_token'];
		if(!empty($user_code) && !empty($user_token) && !empty($input['name']) && !empty($input['email'])){
			$checkToken = $this->Adminmodel->checkUserAuthKey($user_code,$user_token);
			if($checkToken > 0){
                $name   = $input['name'];
				$email = $input['email'];
				$data = array(
					'user_name'=>$name,
					'user_email'=>$email
				);
				$where = array(
					'user_code'=>$user_code,
					'user_token'=>$user_token
				);
				
				$result = $this->Adminmodel->updateUsers($data,$where);
				if($result){
                    $responseList = self::success_response('TRUE','0',MESSAGE_SUCCESS,$result[0]);
                    $this->set_response($responseList,REST_Controller::HTTP_OK); 
				}
				else{
                    $responseList = self::success_response('FALSE','1',MESSAGE_NORECORD);
                    $this->set_response($responseList,REST_Controller::HTTP_OK);
				}
			}else{
                $responseList = self::success_response('FALSE','10',MESSAGE_INVALDTOKEN);
                $this->set_response($responseList,REST_Controller::HTTP_OK);
			}
		}else{
            $responseList = self::success_response('FALSE','2',MESSAGE_PARAMETERMISSING);
            $this->set_response($responseList,REST_Controller::HTTP_OK);
		}
    }
    // insert query
    public function getUserDtail($id){
        $this->db->where('user_mobile',$id);
        $query = $this->db->get('users');
        $result = $query->result();
        if($result){
            return $result[0];
        }
        else{
            return false ;
        }
    }
    // for chnage password 
    public function changePassword_put(){
        $input = json_decode(file_get_contents('php://input'), true); 
        $userId=$input['userId'];
        $userAuthKey=$input['userAuthKey'];
        if(isset($userId) && isset($userAuthKey) && ($this->Adminmodel->checkUserAuthKey($userId,$userAuthKey) > 0)) {

            @$username     = $input['username'];
            @$old_password = $input['old_password'];
            @$new_password =$input['new_password'];
            @$confirm_password = $input['new_password'];
            if($old_password=='' || $new_password==''){
                $responseList = self::success_response('FALSE','2',MESSAGE_PARAMETERMISSING);
                $this->set_response($json_data,REST_Controller::HTTP_OK);  
                
            }
            elseif($new_password!=$confirm_password){
                $responseList = self::success_response('FALSE','8',MESSAGE_PARAMETERMISSING);
                $this->set_response($json_data,REST_Controller::HTTP_OK);   
            }
             elseif($new_password==$old_password){
                $responseList = self::success_response('FALSE','9',MESSAGE_PWDNEWSAME);
                $this->set_response($json_data,REST_Controller::HTTP_OK);  
            }
            else{
                    $data = array(
                        'userId' => $userId ,
                        'user_password' => encryptPassword($old_password),                        
                        );
                    $data1 = array(
                        'userId' => $userId ,
                        'user_password' => encryptPassword($new_password),
                        );    
                    $result = $this->Adminmodel->checkPassword($data,$data1);
                    if($result){
                        $responseList = self::success_response('TRUE','0',MESSAGE_SUCCESS);
                        $this->set_response($responseList,REST_Controller::HTTP_OK); 
                    }
                    else{
                        $responseList = self::success_response('TRUE','10',MESSAGE_INVALIDOPWD);
                        $this->set_response($responseList,REST_Controller::HTTP_OK); 
                }                
            }
        }else{
            $responseList = self::success_response('FALSE','2',MESSAGE_PARAMETERMISSING);
            $this->set_response($json_data,REST_Controller::HTTP_OK);
        }
        
    }
   // for send otp 
    public function sendOtp_post(){
        $input = json_decode(file_get_contents('php://input'), true); 
        if (isset($input['mobile']) && !empty($input['mobile']) && !empty($input['device_type'])
         && !empty($input['otp_type'])){
            
            $mob="/^[1-9][0-9]*$/";
			$device_type = $input['device_type'];
			$otp_type    = $input['otp_type'];
			
            if(!preg_match($mob,$input['mobile']) || strlen($input['mobile'])!='10') {
                $responseList = self::success_response('FALSE','6',MESSAGE_INVALDMOBILE);
                $this->set_response($responseList,REST_Controller::HTTP_OK);
            }else{
                    $min = "1230";
                    $max = "5269";
                    $rand1='1234';
                    // update
                    $data1 = array(
                        'user_otp' =>$rand1,                   
                    );
                    // for checking  				
                    $where = array(
                        'otp_mobile' =>$input['mobile'],  
                        'otp_type'   =>$otp_type,  
                        'otp_device_type_id'=>$device_type 					
                    );
                    // for insert				
                    $data2 = array(
                        'otp_mobile' =>$input['mobile'],
                        'user_otp' =>$rand1, 					
                        'otp_type'   =>$otp_type,  
                        'otp_device_type_id'=>$device_type 					
                    ); 					
                    $message1 = urlencode('OTP from asap is '.$rand1.' . Do not share it with any one.'); // Message text required to deliver on mobile number
                    //$sendSMS = sendMobileSMS($message1,$mobile);
                    $result = $this->Adminmodel->userOtp($where,$data1,$data2);
                    if($result){                                        
                        $responseList = self::success_response('TRUE','0',MESSAGE_OTPSENT);
                        $this->set_response($responseList,REST_Controller::HTTP_OK); 
                    }
                    else{
                        $responseList = self::success_response('TRUE','1',MESSAGE_NORECORD,$result[0]);
                        $this->set_response($responseList,REST_Controller::HTTP_OK); 
                    }
                }  
            }else{
                $responseList = self::success_response('FALSE','2',MESSAGE_PARAMETERMISSING);
                $this->set_response($responseList,REST_Controller::HTTP_OK);
            }
    }    
    // for verify  otp 
    public function verifyOtp_post(){
	    $input = json_decode(file_get_contents('php://input'), true); 
        $mobile = $input['mobile'];
        $otp = $input['otp'];
        $otp_type = $input['otp_type']; 
        $device_type = $input['device_type'];
		$email = $input['email'];
		$name  = $input['name'];
        $password =$input['password'];
        if (isset($mobile) && !empty($mobile) && isset($otp) && !empty($otp) && !empty($device_type) && !empty($otp_type) && isset($email) && !empty($email) && isset($name) && !empty($name) &&
		isset($password) && !empty($password)){ 
            $mob="/^[1-9][0-9]*$/";
            if(!preg_match($mob,$mobile) || strlen($mobile)!='10') {
                $responseList = self::success_response('FALSE','6',MESSAGE_INVALDMOBILE);
                $this->set_response($responseList,REST_Controller::HTTP_OK);
            }else{
               
                $otpCheck= array(
                    'otp_mobile' => $mobile,
                    'user_otp'   => $otp,
		    'otp_type'   => $otp_type,
		    'otp_device_type_id' => $device_type,
                );
                  
                $result = $this->Adminmodel->userVerifyOtp($otpCheck);
                if($result){
							
					$password        = encryptPassword($password);
					$date_join     = date('Y-m-d H:i:s');
					$auth_key      = bin2hex(openssl_random_pseudo_bytes(16));            
					$random1       = rand(102,8596);
					$string2       = str_shuffle('1026358132659');
					$random2       = substr($string2,0,3);
					//$contstr       = "ASAP";
					$userId        = $random1.$random2;
					$regType       = $input['device_type'];            
					$user_login_type_id=1;
					$data = array(
					'user_code'         => $userId,
					'user_token'        => $auth_key,
					'user_name'         => $name ,
					'user_email'        => $email ,             			
					'user_mobile'       => $mobile ,
					'user_password'     => $password,
					'user_createdat'        => $date_join,
					'register_devicetype_id'=>$regType,
					'user_login_type_id'  => $user_login_type_id,
					'user_isactive' => '1',			
					);			
					$checkData = $this->Adminmodel->regUser($data) ;	
                    if($checkData){				    
						$responseList = self::success_response('TRUE','0',MESSAGE_OTPVERIFY);
						$this->set_response($responseList,REST_Controller::HTTP_OK); 
					}else{
						$responseList = self::success_response('FALSE','5',MESSAGE_ERROR);
						$this->set_response($json_data,REST_Controller::HTTP_OK);   
					}
                }
                else{
                    $responseList = self::success_response('TRUE','1',MESSAGE_INVALIDOTP);
                    $this->set_response($responseList,REST_Controller::HTTP_OK); 
                } 
            }
        }else{
            $responseList = self::success_response('FALSE','2',MESSAGE_PARAMETERMISSING);
            $this->set_response($responseList,REST_Controller::HTTP_OK);
        }
    }  
    public function resetPassword_put(){
        $input = json_decode(file_get_contents('php://input'), true); 
        if(isset($input['mobile'])){
            $mobile   = $input['mobile'];
            $password = $input['password'];
            $userCheck = array(
                'user_mobile' => $mobile
            );
            $checkData = $this->Adminmodel->userExist($userCheck);
            if($checkData >0){
                $data1 = array(
                    'user_password' => encryptPassword($password),
                );    
                $result = $this->Adminmodel->resetPassword($mobile,$data1,unserialize(USERCOLUMNLIST)); 
                if($result){
                    $responseList = self::success_response('TRUE','0',MESSAGE_SUCCESS,$result[0]);
                    $this->set_response($responseList,REST_Controller::HTTP_OK); 
                }else{
                    $responseList = self::success_response('FALSE','7',MESSAGE_NOMOBILE);
                    $this->set_response($json_data,REST_Controller::HTTP_OK);
                }
            }
            else{
                $responseList = self::success_response('FALSE','5',MESSAGE_ERROR);
                $this->set_response($json_data,REST_Controller::HTTP_OK);   
            }
        }else{
            $responseList = self::success_response('FALSE','2',MESSAGE_PARAMETERMISSING);
            $this->set_response($json_data,REST_Controller::HTTP_OK);
        }        
    }
    public function categories_get(){       
        $result = $this->Adminmodel->categoryList(unserialize(CATEGORYCOLUMNLIST));
        if($result){
            foreach ($result as $key => $field) {    
                $result[$key]['category_appimage'] = base_url()."uploads/category_appimage/".($field['category_appimage']);
                $result[$key]['category_webimage'] = base_url()."uploads/category_webimage/".($field['category_webimage']);   
            }
            $responseList = self::success_response('SUCCESS','0',MESSAGE_SUCCESS,$result);
            $this->set_response($responseList,REST_Controller::HTTP_OK); 
        }
        else{                    
            $responseList = self::success_response('FALSE','1',MESSAGE_NORECORD);  
            $this->set_response($responseList,REST_Controller::HTTP_OK);
        }
    }
    //  resturant list 
    public function restaurantList_get(){
        //$input = json_decode(file_get_contents('php://input'), true); 
        $userId      = $this->input->get('userId', TRUE);
        $userAuthKey = $this->input->get('user_token', TRUE);
        //$userId ='6129326';
        //$userAuthKey = '11cf8f9df65cfad2ae7c91db0d17cfa8';        
        $lat         = '17.4407862';//$input['lat'];
        $long        = '78.39094';//$input['long'];
        $distance    =  10;
        $chefType    = 1;
        $checkPoint  = 20;// else cond
        $minLimit    = 0;
        $maxLimit    = 100;
        if(isset($lat) && isset($long)) {
           $result2 = $this->Adminmodel->restaurantList($lat,$long,$checkPoint,$minLimit,$maxLimit,$distance);
           //print_r($result2);
           //exit;
           $list = array();
           if($result2){
            $k=0;
            $papularList =array();
            $recomandedList =array() ;
            $topRatedList =array();    
            foreach ($result2 as $key => $field) { 
                    $listdada=$this->Adminmodel->restaurantBranchList($lat,$long,$field['vendor_code'],$distance,1) ; 
                    if($listdada !="0"){                            
                        if(!array_key_exists($listdada[$k]['vendor_code'], $list))
                        {        
                           $ard = array("outlet"=>1);
                           $listdada=array_merge($listdada,$ard);                            
                           array_push($list,$listdada);
                        }                           
                    }
                    $k++;
                        $list[$key]['outlet']  =count(self::countOutlet($lat,$long,$field['vendor_code'],$distance,10));
                        $list[$key]['outletList'] = self::countOutlet($lat,$long,$field['vendor_code'],$distance,10);   
                        $web_logo = $this->Adminmodel->singleColumnData('vendor_code',$field['vendor_code'],'web_logo','vendor');
                        $app_logo =  $this->Adminmodel->singleColumnData('vendor_code',$field['vendor_code'],'app_logo','vendor');
                        $restaurant_name =  $this->Adminmodel->singleColumnData('vendor_code',$field['vendor_code'],'restaurant_name','vendor');
                        $list[$key]['web_logo'] = base_url()."uploads/web_logo/".$web_logo;
                        $list[$key]['app_logo'] = base_url()."uploads/app_logo/".$app_logo; 
                        $list[$key]['web_logo'] = base_url()."uploads/web_logo/".$web_logo;
                        $list[$key]['restaurant_name'] = $restaurant_name; 
                        if($list[$key]['toprated']==1){
                            $topRatedList[] =$list[$key];                           
                            unset($list[$key]);
                        }
                        if($list[$key]['popular']==1){
                            $papularList[] =$list[$key];                           
                            unset($list[$key]);
                        }
                        if($list[$key]['recomanded']==1){
                            $recomandedList[] =$list[$key];
                            unset($list[$key]);
                        }
                        //$list['papularList']   = $papularList;
                        //$list['recomandedList']= $recomandedList;
                       // $list['topRatedList']= $topRatedList;
                     
            } 
               
                // //$mainList = array($papularList);
                // //array_push($mainList,$papularList);
                // array_push($mainList,$topRatedList);
                // array_push($mainList,$recomandedList);
                // //$responseList = self::success_response('SUCCESS','0',MESSAGE_SUCCESS,$list);
                $json_data['status']='SUCCESS';
                $json_data['responseCode']='0';
                $json_data['message']=MESSAGE_SUCCESS;
                $json_data['papularList']=$papularList;
                $json_data['recomandedList']=$recomandedList;
                $json_data['topRatedList']=$topRatedList;
                $this->set_response($json_data,REST_Controller::HTTP_OK); 
               
            }
            else{
                $responseList = self::success_response('FALSE','1',MESSAGE_NORECORD);
                $this->set_response($responseList,REST_Controller::HTTP_OK); 
            }
        } else{
            $responseList = self::success_response('FALSE','2',MESSAGE_PARAMETERMISSING);
            $this->set_response($responseList,REST_Controller::HTTP_OK);
        }           
    }
    
    // for  restaurant itemList 
    
    public function restaurantItemList_post(){
        $input = json_decode(file_get_contents('php://input'), true); 
        $userId      = $input['userId'];
        $userAuthKey = $input['userAuthKey'];
        $userId ='OMLY_tak954';
        $userAuthKey = 'a8c48a1db836eb0e6b048fd629a0a9f3';        
        $vendorId = $input['vendorId'];
        $branchId = $input['branchId'];
        $catId    = $input['catId'];
        $dishType = $input['dishType'];
        $search   = $input['search'];  
        
        if(isset($userId) && isset($userAuthKey) && ($this->Adminmodel->checkUserAuthKey($userId,$userAuthKey) > 0)  && isset($vendorId) && isset($branchId)) {
           $result = $this->Adminmodel->restaurantItemList($vendorId,$branchId,$catId,$dishType,$search);
          
           if($result){
            
                 foreach ($result as $key => $field) {
                       
                        $result[$key]['productImage'] = base_url().'uploads/productImage/'.$field['productImage'];  
                        $result[$key]['addOnproductList'] =  $this->Adminmodel->productAddon($branchId,$field['productId']); 
                }
                $json_data['status']='Success';
                $json_data['responseCode']=0;
                $json_data['message']="Restaurant Item List Retrive Successfully.";
                $json_data['list']=$result;
                $this->set_response($json_data,REST_Controller::HTTP_OK); 
            }
            else{
                $json_data['status']='Fail';
                $json_data['responseCode']=1;
                $json_data['message']=array("Records not found");
                $this->set_response($json_data,REST_Controller::HTTP_OK);
            }
        } else{
                $json_data['status']='FALSE';
                $json_data['responseCode']=4;               
                $json_data['message']="Please enter all the mandatory fields";
                $this->set_response($json_data,REST_Controller::HTTP_OK);
        }           
    }  
    
    // for branch related data 

    public function itemList_post(){
        $input = json_decode(file_get_contents('php://input'), true);        
        $vendorId = $input['vendorId'];
        $branchId = $input['branchId'];
        $catId    = $input['catId'];
        $dishType = $input['dishType']; // Veg or nonVeg 
        $search   = $input['search'];         
        if(isset($vendorId) && isset($branchId)) {
        // about restaurants 
        $branchDetail = $this->Adminmodel->branchDetail($vendorId,$branchId); 
        if($branchDetail){           
            foreach ($branchDetail as $keyIndex => $fieldData) { 
                $app_logo =  $this->Adminmodel->singleColumnData('vendor_code',$fieldData['vendor_code'],'app_logo','vendor');
                $restaurant_name =  $this->Adminmodel->singleColumnData('vendor_code',$fieldData['vendor_code'],'restaurant_name','vendor');
                $branchDetail[$keyIndex]['app_logo'] = base_url()."uploads/app_logo/".$app_logo; 
                $branchDetail[$keyIndex]['restaurant_name'] = $restaurant_name;  
            }
        }else{
            $branchDetail = array();
        }

        $resultRecomanded = $this->Adminmodel->restaurantProductRecomanded($vendorId,$branchId);         
        if($resultRecomanded){           
            foreach ($resultRecomanded as $keynew => $fieldnew) { 
                $categoryName = $this->Adminmodel->getSingleColumnName($fieldnew['product_cat_id'],'id','category_name','category'); 
                $resultRecomanded[$keynew]['category']= $categoryName ;
                $subcategoryName = $this->Adminmodel->getSingleColumnName($fieldnew['product_sub_cat_id'],'id','subcategory_name','subcategory'); 
                $resultRecomanded[$keynew]['subcategory']= $subcategoryName ;
                $resultRecomanded[$keynew]['product_name']= $this->Adminmodel->getSingleColumnName($fieldnew['product_id'],'id','product_name','vendor_products'); 
                $image = $this->Adminmodel->getSingleColumnName($fieldnew['product_id'],'id','product_image','vendor_products'); 
                $resultRecomanded[$keynew]['image']= base_url()."uploads/product_image/".$image ;
                $resultRecomanded[$keynew]['priceList']= self::getPriceList($vendorId,$branchId,$fieldnew['product_id']);

            }
        }else{
            $resultRecomanded = array();
        }                 
          if($branchDetail){        
                $json_data['status']='Success';
                $json_data['responseCode']=0;
                $json_data['message']="Restaurant Item List Retrive Successfully.";
                $json_data['branchDetail']=$branchDetail[0];
                $json_data['recomandedList']=$resultRecomanded;
                $this->set_response($json_data,REST_Controller::HTTP_OK); 
            }
            else{
                $json_data['status']='Fail';
                $json_data['responseCode']=1;
                $json_data['message']=array("Records not found");
                $this->set_response($json_data,REST_Controller::HTTP_OK);
            }
        } else{
                $json_data['status']='FALSE';
                $json_data['responseCode']=4;               
                $json_data['message']="Please enter all the mandatory fields";
                $this->set_response($json_data,REST_Controller::HTTP_OK);
        }           
    }

    // for item detail only


    public function itemDetailList_post(){
        $input = json_decode(file_get_contents('php://input'), true);        
        $vendorId = $input['vendorId'];
        $branchId = $input['branchId'];
        $catId    = $input['catId'];
        $dishType = $input['dishType']; // Veg or nonVeg 
        $search   = $input['search'];         
        if(isset($vendorId) && isset($branchId)) {
           $result = $this->Adminmodel->restaurantCatList($vendorId,$branchId);          
           if($result){           
                 foreach ($result as $key => $field) {  
                    $titalSubcat = count(self::getrestaurantSubCatList($vendorId,$branchId,$field['product_cat_id'])) ;
                    $categoryName = $this->Adminmodel->getSingleColumnName($field['product_cat_id'],'id','category_name','category'); 
                    $result[$key]['category']= $categoryName ;  
                    if($titalSubcat > 0) {
                    $result[$key]['subcatCount'] = $titalSubcat;    
                    $result[$key]['subcatList'] = self::getrestaurantSubCatList($vendorId,$branchId,$field['product_cat_id']);  
                    }else{
                        $result[$key]['subcatCount'] = 0;
                        $result[$key]['subcatList'] = array();
                        $subcat = NULL ;
                        $result[$key]['productList']= self::getProductList($vendorId,$branchId,$field['product_cat_id'],$subcat);

                    }
                }
                $json_data['status']='Success';
                $json_data['responseCode']=0;
                $json_data['message']="Restaurant Item List Retrive Successfully.";
                $json_data['list']=$result;
                $this->set_response($json_data,REST_Controller::HTTP_OK); 
            }
            else{
                $json_data['status']='Fail';
                $json_data['responseCode']=1;
                $json_data['message']=array("Records not found");
                $this->set_response($json_data,REST_Controller::HTTP_OK);
            }
        } else{
                $json_data['status']='FALSE';
                $json_data['responseCode']=4;               
                $json_data['message']="Please enter all the mandatory fields";
                $this->set_response($json_data,REST_Controller::HTTP_OK);
        }           
    }

    // setting detail 
    public function getSettingDetail($column){
        $this->db->where('id','1');
        $query = $this->db->get('basic_settings');
        $result = $query->row();
        if($result){
            return $result->$column;
        }
        else{
            return "" ;
        }
    }
    
    public function countOutlet($lat,$long,$venid,$distance,$nogrp){
        $all_list = array();
        $qyery= $this->db->query("CALL getBrancheList($lat,$long,$venid,$distance,$nogrp)");
        $num=$qyery->num_rows();        
        if($num > 0){
            $result2 = $qyery->result_array();
            mysqli_next_result($this->db->conn_id);
            foreach ($result2 as $key => $field) {
                if($all_list[$key]['vendor_code'] !=$result2[$key]['vendor_code']){
                    $all_list[] = $result2[$key];
                }
            }    
            return $all_list;
        }
        else{
            $array= array();
            return $array ;
        }
    }

    public function getrestaurantSubCatList($vendorId,$branchId,$catId){          
        $result = $this->Adminmodel->restaurantSubCatList($vendorId,$branchId,$catId);
        if($result){
            foreach ($result as $key => $field) {
                $subcategoryName = $this->Adminmodel->getSingleColumnName($field['product_sub_cat_id'],'id','subcategory_name','subcategory'); 
                $result[$key]['subcategory']= $subcategoryName ; 
                $result[$key]['productList']= self::getProductList($vendorId,$branchId,$catId,$field['product_sub_cat_id']);
            }  
            return $result ; 
        }else{
            $emptyArr = array();
            return $emptyArr ;
        }
          
    }

    public function getProductList($vendorId,$branchId,$catId,$subcat){  
        $product_result = $this->Adminmodel->restaurantproductList($vendorId,$branchId,$catId,@$subcat);
        if($product_result){
            foreach ($product_result as $key => $field) {                
                $product_result[$key]['product_name']= $this->Adminmodel->getSingleColumnName($field['product_id'],'id','product_name','vendor_products'); 
                $product_result[$key]['priceList']= self::getPriceList($vendorId,$branchId,$field['product_id']);
            }  
            return $product_result ; 
        }else{
            $emptyArr = array();
            return $emptyArr ;
        }
    }

    public function getPriceList($vendorId,$branchId,$pid){  
        $price_result = $this->Adminmodel->restaurantProductPriceList($vendorId,$branchId,$pid);
        if($price_result){
            foreach ($price_result as $key2 => $field2) {               
                $price_result[$key2]['product_weight']= $this->Adminmodel->getSingleColumnName($field2['product_weight_id'],'id','product_weight','prodcut_weights'); 
            }  
            return $price_result ; 
        }else{
            $emptyArr = array();
            return $emptyArr ;
        }
    }


    // for banner
    public function bannerList_get(){
        $where = array(
            "isactive"=>0
        );
        $columnList = array(
            "id","image"
        );
        $result = $this->Adminmodel->fetchAllrecords('banners',$where,$columnList);
        if($result){          
            foreach ($result as $keynew => $fieldnew) { 
                $result[$keynew]['image']= base_url()."uploads/banner_webimage/".$fieldnew['image'] ;
            }
            $responseList = self::success_response('TRUE','0',MESSAGE_SUCCESS,$result);
            $this->set_response($responseList,REST_Controller::HTTP_OK); 
        }
        else{
            $responseList = self::success_response('FALSE','1',MESSAGE_NORECORD);
            $this->set_response($responseList,REST_Controller::HTTP_OK); 
        }
    }

    public function addressList_get(){
        $userId      = $this->input->get('userId', TRUE);
        $userAuthKey = $this->input->get('user_token', TRUE);
        $id = $this->input->get('id', TRUE);
        $default_address = $this->input->get('default_address', TRUE);
        if($userId !="" && $userAuthKey !="") {
        $where = array(
            "isactive"=>0,
            "user_id" =>$userId
        );
        if($id !=""){
            $where['id'] = $id;
        }
        if($default_address !=""){
            $where['default_address'] = 1;
        }
        $columnList = array(
            "id","user_id","house_no","street_no","area","city","land_mark","address_type","default_address"
        );
        $result = $this->Adminmodel->fetchAllrecords('address_list',$where,$columnList);
            if($result){          
                foreach ($result as $keynew => $fieldnew) { 
                    if($fieldnew['address_type']==1){
                        $address_type ="Home";
                    }
                    elseif($fieldnew['address_type']==2){
                        $address_type ="Office";
                    }
                    elseif($fieldnew['address_type']==3){
                        $address_type ="Other";
                    }
                    else{
                        $address_type ="Other";
                    }
                    $result[$keynew]['address_type']= $address_type ;
                }
                $responseList = self::success_response('TRUE','0',MESSAGE_SUCCESS,$result);
                $this->set_response($responseList,REST_Controller::HTTP_OK); 
            }
            else{
                $responseList = self::success_response('FALSE','1',MESSAGE_NORECORD);
                $this->set_response($responseList,REST_Controller::HTTP_OK); 
            }
        }else{
            $responseList = self::success_response('FALSE','2',MESSAGE_PARAMETERMISSING);
            $this->set_response($responseList,REST_Controller::HTTP_OK);
        }
    }

    // for add address 
    public function addAddress_post(){  
        $input = json_decode(file_get_contents('php://input'), true); 
        $user_code    = $input['userId'] !="" ? $input['userId']:'';
        $user_token   = $input['user_token']!="" ? $input['user_token']:'';
        $house_no     = $input['house_no']!="" ? $input['house_no']:'';
        $street_no    = $input['street_no']!="" ? $input['street_no']:'';
        $area         = $input['area']!="" ? $input['area']:'';  
        $city         = $input['city']!="" ? $input['city']:'';
        $land_mark    = $input['land_mark']!="" ? $input['land_mark']:'';
        $address_type = $input['address_type']!="" ? $input['address_type']:'0';

		if(!empty($user_code) && !empty($user_token) && !empty($user_token) && !empty($house_no)  && !empty($street_no)  && !empty($area)  && !empty($city)  && !empty($land_mark) && !empty($address_type)){
			$checkToken = $this->Adminmodel->checkUserAuthKey($user_code,$user_token);                      
			if($checkToken > 0){	
                $insertData =array(
                    user_id => $user_code ,
                    house_no => $house_no,
                    street_no => $street_no,
                    area => $area ,
                    city => $city ,
                    land_mark => $land_mark,
                    address_type =>$address_type
                );			
				$result = $this->Adminmodel->queInsert('address_list',$insertData);                                
				if($result){                    
                    $responseList = self::success_response('TRUE','0',MESSAGE_INSERTSUCESS);
                    $this->set_response($responseList,REST_Controller::HTTP_OK); 
				}
				else{
                    $responseList = self::success_response('FALSE','1',MESSAGE_ERROR);
                    $this->set_response($responseList,REST_Controller::HTTP_OK);
				}
			}else{
                $responseList = self::success_response('FALSE','12',MESSAGE_INVALDTOKEN);
                $this->set_response($responseList,REST_Controller::HTTP_OK);
			}
		}else{
            $responseList = self::success_response('FALSE','2',MESSAGE_PARAMETERMISSING);
            $this->set_response($responseList,REST_Controller::HTTP_OK);
		}
    }    

   // update address 
    public function editAddress_post(){  
        $input = json_decode(file_get_contents('php://input'), true); 
        $id           = $input['id'];
        $user_code    = $input['userId'] !="" ? $input['userId']:'';
        $user_token   = $input['user_token']!="" ? $input['user_token']:'';
        $house_no     = $input['house_no']!="" ? $input['house_no']:'';
        $street_no    = $input['street_no']!="" ? $input['street_no']:'';
        $area         = $input['area']!="" ? $input['area']:'';  
        $city         = $input['city']!="" ? $input['city']:'';
        $land_mark    = $input['land_mark']!="" ? $input['land_mark']:'';
        $address_type = $input['address_type']!="" ? $input['address_type']:'0';

        if(!empty($id) &&  !empty($user_code) && !empty($user_token) && !empty($user_token) && !empty($house_no)  && !empty($street_no)  && !empty($area)  && !empty($city)  && !empty($land_mark) && !empty($address_type)){
            $checkToken = $this->Adminmodel->checkUserAuthKey($user_code,$user_token);                      
            if($checkToken > 0){	
                $data = array (
                    house_no => $house_no,
                    street_no => $street_no,
                    area => $area ,
                    city => $city ,
                    land_mark => $land_mark,
                    address_type =>$address_type
                );	
                $where_data = array (
                    user_id => $user_code,
                    id => $id
                );		
                $result = $this->Adminmodel->updateAsapRecord('address_list',$data,$where_data);                                      
                if($result){
                    $responseList = self::success_response('TRUE','0',MESSAGE_SUCCESSUPDATE);
                    $this->set_response($responseList,REST_Controller::HTTP_OK); 
                }
                else{
                    $responseList = self::success_response('FALSE','1',MESSAGE_ERROR);
                    $this->set_response($responseList,REST_Controller::HTTP_OK);
                }
            }else{
                $responseList = self::success_response('FALSE','12',MESSAGE_INVALDTOKEN);
                $this->set_response($responseList,REST_Controller::HTTP_OK);
            }
        }else{
            $responseList = self::success_response('FALSE','2',MESSAGE_PARAMETERMISSING);
            $this->set_response($responseList,REST_Controller::HTTP_OK);
        }
    } 

    public function defaultAddress_post(){  
        $input = json_decode(file_get_contents('php://input'), true); 
        $id           = $input['id'];
        $user_code    = $input['userId'] !="" ? $input['userId']:'';
        $user_token   = $input['user_token']!="" ? $input['user_token']:'';
        if(!empty($id) &&  !empty($user_code) && !empty($user_token) && !empty($user_token)){
            $checkToken = $this->Adminmodel->checkUserAuthKey($user_code,$user_token);                      
            if($checkToken > 0){	
                $data =array(
                    default_address => 1,
                );	
                $where_data = array(
                    user_id => $user_code ,
                    id => $id
                );
               	
                $result = $this->Adminmodel->updateDefaultAddress('address_list',$data,$where_data);                                      
                if($result){
                    $responseList = self::success_response('TRUE','0',MESSAGE_SUCCESSUPDATE);
                    $this->set_response($responseList,REST_Controller::HTTP_OK); 
                }
                else{
                    $responseList = self::success_response('FALSE','1',MESSAGE_ERROR);
                    $this->set_response($responseList,REST_Controller::HTTP_OK);
                }
            }else{
                $responseList = self::success_response('FALSE','12',MESSAGE_INVALDTOKEN);
                $this->set_response($responseList,REST_Controller::HTTP_OK);
            }
        }else{
            $responseList = self::success_response('FALSE','2',MESSAGE_PARAMETERMISSING);
            $this->set_response($responseList,REST_Controller::HTTP_OK);
        }
    } 
    public function userList_post(){
        $input = json_decode(file_get_contents('php://input'), true); 
        $id           = $input['id'];
        $user_code    = $input['userId'] !="" ? $input['userId']:'';
        $user_token   = $input['user_token']!="" ? $input['user_token']:'';
        if($user_code !="" && $user_token !="") {
        $where = array(
            "user_isactive"=>0
        );        
        $columnList = array("id","user_code","user_name");
        $result = $this->Adminmodel->fetchAllrecords('users',$where,$columnList);
            if($result){   
                foreach ($result as $keynew => $fieldnew) { 
                    if (in_array($user_code, $fieldnew)) {
                        unset($result[$keynew]);
                    }
                }       
                $responseList = self::success_response('TRUE','0',MESSAGE_SUCCESS,$result);
                $this->set_response($responseList,REST_Controller::HTTP_OK); 
            }
            else{
                $responseList = self::success_response('FALSE','1',MESSAGE_NORECORD);
                $this->set_response($responseList,REST_Controller::HTTP_OK); 
            }
        }else{
            $responseList = self::success_response('FALSE','2',MESSAGE_PARAMETERMISSING);
            $this->set_response($responseList,REST_Controller::HTTP_OK);
        }
    }
    // for new group 
     // for add address 
     public function addgroup_post(){  
        $input = json_decode(file_get_contents('php://input'), true); 
        $user_code    = $input['userId'] !="" ? $input['userId']:'';
        $user_token   = $input['user_token']!="" ? $input['user_token']:'';
        $owner_id  = $input['owner_id']!="" ? $input['owner_id']:'';
        $group_name  = $input['group_name']!="" ? $input['group_name']:'asap group';
		if(!empty($user_code) && !empty($user_token) && !empty($user_token) && !empty($owner_id)){
			$checkToken = $this->Adminmodel->checkUserAuthKey($user_code,$user_token);                      
			if($checkToken > 0){
                $min =1089 ;
                $max = 9685;
                $groupId = rand($min,$max) ;	
                $insertData =array(
                    group_id => $groupId,
                    owner_id => $owner_id,
                    group_name =>$group_name
                );			
				$result = $this->Adminmodel->queInsert('asap_group',$insertData);                                
				if($result){                                                       
                    $responseList = self::success_responseSingle('TRUE','0',MESSAGE_INSERTSUCESS,'groupd_id',$groupId);
                    $this->set_response($responseList,REST_Controller::HTTP_OK); 
				}
				else{
                    $responseList = self::success_response('FALSE','1',MESSAGE_ERROR);
                    $this->set_response($responseList,REST_Controller::HTTP_OK);
				}
			}else{
                $responseList = self::success_response('FALSE','12',MESSAGE_INVALDTOKEN);
                $this->set_response($responseList,REST_Controller::HTTP_OK);
			}
		}else{
            $responseList = self::success_response('FALSE','2',MESSAGE_PARAMETERMISSING);
            $this->set_response($responseList,REST_Controller::HTTP_OK);
		}
    } 

    // add from group
    public function addgrouplist_post(){  
        $emptyArr = array(); 
        $input = json_decode(file_get_contents('php://input'), true); 
        $user_code    = $input['userId'] !="" ? $input['userId']:'';
        $user_token   = $input['user_token']!="" ? $input['user_token']:'';
        $owner_id  = $input['owner_id']!="" ? $input['owner_id']:'';
        $group_id  = $input['group_id'];
        $userList  = $input['userList'];
        $countUser = count($userList);
		if(!empty($user_code) && !empty($user_token) && !empty($userList)  && !empty($owner_id)  && !empty($group_id)){
			$checkToken = $this->Adminmodel->checkUserAuthKey($user_code,$user_token);                      
			if($checkToken > 0){         
                       $checkExist = array(
                        'group_id'=> $group_id ,
                        'user_id' => $userList
                    );  
            $result_num = $this->Adminmodel->groupUserExist($checkExist);   
            if($result_num > 0){
                $where = array(
                    "isactive"=> 0 ,
                    "group_id" => $group_id
                    );
                $columnList = array("id","group_id","owner_id","user_id");
                $result = $this->Adminmodel->fetchAllrecords('group_detail',$where,$columnList);
                if($result){   
                    foreach ($result as $keynew => $fieldnew) { 
                        if($fieldnew['user_id'] !=""){
                            $result[$keynew]['user_name']= $this->Adminmodel->getSingleColumnName($fieldnew['user_id'],'user_code','user_name','users');
                        }
                        $result[$keynew]['group_name']= $this->Adminmodel->getSingleColumnName($fieldnew['group_id'],'group_id','group_name','asap_group') ;
                    }  
                }  
                $json_data['status']='TRUE';
                $json_data['responseCode']=3;
                $json_data['message']="user Already Added";
                $json_data['list']=replace_empty_values($result);
                $this->set_response($json_data,REST_Controller::HTTP_OK); 
            }    
            else{
                        $insertData = array(
                            'group_id'=> $group_id ,
                            'owner_id'=> $owner_id,
                            'user_id' => $userList
                        );                                                
                        $result = $this->Adminmodel->queInsert('group_detail',$insertData); 
                                               
				if($result > 0){                      
                    $where = array(
                        "isactive"=> 0 ,
                        "group_id" => $group_id
                        );
                    $columnList = array("id","group_id","owner_id","user_id");
                    $result = $this->Adminmodel->fetchAllrecords('group_detail',$where,$columnList);
                    if($result){   
                        foreach ($result as $keynew => $fieldnew) { 
                            if($fieldnew['user_id'] !=""){
                                $result[$keynew]['user_name']= $this->Adminmodel->getSingleColumnName($fieldnew['user_id'],'user_code','user_name','users');
                            }
                            $result[$keynew]['group_name']= $this->Adminmodel->getSingleColumnName($fieldnew['group_id'],'group_id','group_name','asap_group') ;
                        }  
                    }                                     
                    $responseList = self::success_response('TRUE','0',MESSAGE_INSERTSUCESS,$result);
                    $this->set_response($responseList,REST_Controller::HTTP_OK); 
				}
				else{
                    $responseList = self::success_response('FALSE','1',MESSAGE_ERROR);
                    $this->set_response($responseList,REST_Controller::HTTP_OK);
                }
                
            }


			}else{
                $responseList = self::success_response('FALSE','12',MESSAGE_INVALDTOKEN);
                $this->set_response($responseList,REST_Controller::HTTP_OK);
			}
		}else{
            $responseList = self::success_response('FALSE','2',MESSAGE_PARAMETERMISSING);
            $this->set_response($responseList,REST_Controller::HTTP_OK);
		}
    }
    // group detail
    public function groupDetail_post(){
        $input = json_decode(file_get_contents('php://input'), true); 
        $id           = $input['id'];
        $user_code    = $input['userId'] !="" ? $input['userId']:'';
        $user_token   = $input['user_token']!="" ? $input['user_token']:'';
        $group_id  = $input['group_id'];
        if(!empty($user_code) && !empty($user_token) && !empty($group_id)){
			$checkToken = $this->Adminmodel->checkUserAuthKey($user_code,$user_token);                      
			if($checkToken > 0){              
            $where = array(
                "isactive"=> 0 ,
                "group_id" => $group_id
            );        
            $columnList = array("id","group_id","owner_id","user_id");
            $result = $this->Adminmodel->fetchAllrecords('group_detail',$where,$columnList);
                if($result){   
                    foreach ($result as $keynew => $fieldnew) { 
                        if($fieldnew['user_id'] !=""){
    
                            $result[$keynew]['user_name']= $this->Adminmodel->getSingleColumnName($fieldnew['user_id'],'user_code','user_name','users');
                        }
                        $result[$keynew]['group_name']= $this->Adminmodel->getSingleColumnName($fieldnew['group_id'],'group_id','group_name','asap_group') ;
                    }       
                    $responseList = self::success_response('TRUE','0',MESSAGE_SUCCESS,$result);
                    $this->set_response($responseList,REST_Controller::HTTP_OK); 
                }
                else{
                    $responseList = self::success_response('FALSE','1',MESSAGE_NORECORD);
                    $this->set_response($responseList,REST_Controller::HTTP_OK); 
                }
            }
            else{
                $responseList = self::success_response('FALSE','12',MESSAGE_INVALDTOKEN);
                $this->set_response($responseList,REST_Controller::HTTP_OK);
            }
        }else{
            $responseList = self::success_response('FALSE','2',MESSAGE_PARAMETERMISSING);
            $this->set_response($responseList,REST_Controller::HTTP_OK);
        }
    }

     // group detail by ownerId
     public function groupList_post(){
        $input = json_decode(file_get_contents('php://input'), true); 
        $id           = $input['id'];
        $user_code    = $input['userId'] !="" ? $input['userId']:'';
        $user_token   = $input['user_token']!="" ? $input['user_token']:'';
        $owner_id  = $input['owner_id'];
        if(!empty($user_code) && !empty($user_token) && !empty($owner_id)){
			$checkToken = $this->Adminmodel->checkUserAuthKey($user_code,$user_token);                      
			if($checkToken > 0){              
            $where = array(
                "isactive"=> 0 ,
                "owner_id" => $owner_id
            );        
            $columnList = array("id","group_id","owner_id","user_id");
            $result = $this->Adminmodel->fetchAllrecords_byGroup('group_detail',$where,$columnList);
                if($result){   
                    foreach ($result as $keynew => $fieldnew) { 
                        if($fieldnew['user_id'] !=""){
                            $result[$keynew]['user_name']= $this->Adminmodel->getSingleColumnName($fieldnew['user_id'],'user_code','user_name','users');
                        }
                        $result[$keynew]['group_name']= $this->Adminmodel->getSingleColumnName($fieldnew['group_id'],'group_id','group_name','asap_group') ;                        
                        $where2 = array(
                            "isactive"=> 0 ,
                            "group_id" => $fieldnew['group_id']
                        );        
                        $columnList2 = array("id","user_id");
                        $result2 = $this->Adminmodel->fetchAllrecords('group_detail',$where2,$columnList2);
                            if($result2){   
                                foreach ($result2 as $keynew2 => $fieldnew2) { 
                                    if($fieldnew2['user_id'] !=""){
                
                                        $result2[$keynew2]['user_name']= $this->Adminmodel->getSingleColumnName($fieldnew2['user_id'],'user_code','user_name','users');
                                    }
                                } 
                        }
                        $result[$keynew]['group_userList'] =  $result2 ;  
                }     
                    $responseList = self::success_response('TRUE','0',MESSAGE_SUCCESS,$result);
                    $this->set_response($responseList,REST_Controller::HTTP_OK); 
                }
                else{
                    $responseList = self::success_response('FALSE','1',MESSAGE_NORECORD);
                    $this->set_response($responseList,REST_Controller::HTTP_OK); 
                }
            }
            else{
                $responseList = self::success_response('FALSE','12',MESSAGE_INVALDTOKEN);
                $this->set_response($responseList,REST_Controller::HTTP_OK);
            }
        }else{
            $responseList = self::success_response('FALSE','2',MESSAGE_PARAMETERMISSING);
            $this->set_response($responseList,REST_Controller::HTTP_OK);
        }
    }


      // group detail
      public function groupPrimary_post(){
        $input = json_decode(file_get_contents('php://input'), true); 
        $id           = $input['id'];
        $user_code    = $input['userId'] !="" ? $input['userId']:'';
        $user_token   = $input['user_token']!="" ? $input['user_token']:'';
        $group_id  = $input['group_id'];
        if(!empty($user_code) && !empty($user_token) && !empty($group_id)){
			$checkToken = $this->Adminmodel->checkUserAuthKey($user_code,$user_token);                      
			if($checkToken > 0){              
            $where = array(
                "isactive"=> 0 ,
                "group_id" => $group_id
            );        
            $columnList = array("id","group_id","owner_id");
            $result = $this->Adminmodel->fetchAllrecords('asap_group',$where,$columnList);
                if($result){   
                    foreach ($result as $keynew => $fieldnew) {                        
                        $result[$keynew]['group_name']= $this->Adminmodel->getSingleColumnName($fieldnew['group_id'],'group_id','group_name','asap_group') ;
                    }       
                    $responseList = self::success_response('TRUE','0',MESSAGE_SUCCESS,$result);
                    $this->set_response($responseList,REST_Controller::HTTP_OK); 
                }
                else{
                    $responseList = self::success_response('FALSE','1',MESSAGE_NORECORD);
                    $this->set_response($responseList,REST_Controller::HTTP_OK); 
                }
            }
            else{
                $responseList = self::success_response('FALSE','12',MESSAGE_INVALDTOKEN);
                $this->set_response($responseList,REST_Controller::HTTP_OK);
            }
        }else{
            $responseList = self::success_response('FALSE','2',MESSAGE_PARAMETERMISSING);
            $this->set_response($responseList,REST_Controller::HTTP_OK);
        }
    }
    
    // delete group from members
     // for add group member
     public function deleteUserFromGroup_post(){  
        $input = json_decode(file_get_contents('php://input'), true); 
        $user_code    = $input['userId'] !="" ? $input['userId']:'';
        $user_token   = $input['user_token']!="" ? $input['user_token']:'';
        $id  = $input['id'];
        $group_id = $input['group_id'];
		if(!empty($user_code) && !empty($user_token) && !empty($user_token) && !empty($id) && !empty($group_id)){
			$checkToken = $this->Adminmodel->checkUserAuthKey($user_code,$user_token);                      
			if($checkToken > 0){	                		
				$result = $this->Adminmodel->delRow($id,'group_detail');                                
				if($result){    
                    $where = array(
                        "isactive"=> 0 ,
                        "group_id" => $group_id
                    );
                    $columnList = array("id","group_id","owner_id","user_id");
                    $result = $this->Adminmodel->fetchAllrecords('group_detail',$where,$columnList);
                    if($result){   
                        foreach ($result as $keynew => $fieldnew) { 
                            if($fieldnew['user_id'] !=""){
                                $result[$keynew]['user_name']= $this->Adminmodel->getSingleColumnName($fieldnew['user_id'],'user_code','user_name','users');
                            }
                            $result[$keynew]['group_name']= $this->Adminmodel->getSingleColumnName($fieldnew['group_id'],'group_id','group_name','asap_group') ;
                        }  
                    }                    
                    // $responseList = self::success_responseSingle('TRUE','0',MESSAGE_SUCCESSREMOVE,'groupd_id',$group_id);
                    // $this->set_response($responseList,REST_Controller::HTTP_OK); 

                    $responseList = self::success_response('TRUE','0',MESSAGE_SUCCESSREMOVE,$result);
                    $this->set_response($responseList,REST_Controller::HTTP_OK);



				}
				else{
                    $responseList = self::success_response('FALSE','1',MESSAGE_ERROR);
                    $this->set_response($responseList,REST_Controller::HTTP_OK);
				}
			}else{
                $responseList = self::success_response('FALSE','12',MESSAGE_INVALDTOKEN);
                $this->set_response($responseList,REST_Controller::HTTP_OK);
			}
		}else{
            $responseList = self::success_response('FALSE','2',MESSAGE_PARAMETERMISSING);
            $this->set_response($responseList,REST_Controller::HTTP_OK);
		}
    }
    
     // for add group member
     public function saveorderList_post(){  
        $emptyArr = array(); 
        $input = json_decode(file_get_contents('php://input'), true); 
        $user_code    = $input['userId'] !="" ? $input['userId']:'';
        $user_token   = $input['user_token']!="" ? $input['user_token']:'';
        $owner_id  = $input['owner_id']!="" ? $input['owner_id']:'';
        $group_id  = $input['group_id'];
        $ship_id  = $input['group_id'];
        $sub_total = $input['sub_total'];
        $discount = $input['discount']? $input['discount']:'0';
        $service_tax = $input['service_tax']? $input['service_tax']:'0';
        $delivery_charges = $input['delivery_charges']? $input['delivery_charges']:'0';
        $grand_total = $input['grand_total'];
        $payment_mehthod = $input['payment_mehthod']? $input['payment_mehthod']:'0';
        $payment_status = $input['payment_status']? $input['payment_status']:'0';
        // product details
        $product_list = $input['product_list'];
        $device_ype   = $input['device_ype'];
        $countProduct = count($product_list);
		if(!empty($user_code) && !empty($user_token) && !empty($owner_id)  && ($countProduct > 0) && !empty($group_id) && !empty($ship_id) &&  !empty($sub_total) 
         && !empty($grand_total) && !empty($payment_mehthod)){
			$checkToken = $this->Adminmodel->checkUserAuthKey($user_code,$user_token);                      
			if($checkToken > 0){   
                $lastorderId  =  $this->Adminmodel->lastOrderId(); 
                $newOrderId = $this->Adminmodel->generateOrderId($lastorderId);
                $transaction_id = 'TRANC'.$newOrderId ;
                $dataOrder = array(
                    'order_id' =>$newOrderId,   
                    'group_id'=> $group_id ,
                    'owner_id'=> $owner_id,
                    'transaction_id' => $transaction_id,
                    'ship_id' => $ship_id,
                    'sub_total'=> $sub_total,
                    'discount' => $discount,
                    'service_tax'=>$service_tax,
                    'delivery_charges'=>$delivery_charges,
                    'grand_total'=> $grand_total,
                    'payment_mehthod'=> $payment_mehthod
                );
                $insert = 0;
                $result = $this->Adminmodel->queInsert('order_details',$dataOrder); 
                if($result){                
                    if($countProduct > 0) {
                        $insert = 0;
                        for($v=0; $v < $countProduct; $v++) {                        
                            $insertDataOrder = array(
                                'order_id' =>$newOrderId,
                                'group_id'   => $group_id ,
                                'owner_id'   => $owner_id,
                                'product_id' => $product_list[$v]['product_id'],
                                'product_weight' => $product_list[$v]['product_weight'],
                                'product_price' => $product_list[$v]['product_price'],
                                'product_quantity' => $product_list[$v]['product_quantity'],
                                'vendor_id' => $product_list[$v]['vendor_id'],
                                'branch_id' => $product_list[$v]['branch_id'],
                                'device_ype' => $device_ype,
                            );                                                
                            $result_order = $this->Adminmodel->queInsert('order_product_details',$insertDataOrder); 
                            if($result_order){
                                $insert = $insert+1;
                            }
                        }
                    } 
                    
                }                             
				if($insert > 0){                    
                    $responseList = self::success_response('TRUE','0',MESSAGE_INSERTSUCESS);
                    $this->set_response($responseList,REST_Controller::HTTP_OK); 
				}
				else{
                    $responseList = self::success_response('FALSE','1',MESSAGE_ERROR);
                    $this->set_response($responseList,REST_Controller::HTTP_OK);
				}
			}else{
                $responseList = self::success_response('FALSE','12',MESSAGE_INVALDTOKEN);
                $this->set_response($responseList,REST_Controller::HTTP_OK);
			}
		}else{
            $responseList = self::success_response('FALSE','2',MESSAGE_PARAMETERMISSING);
            $this->set_response($responseList,REST_Controller::HTTP_OK);
		}
    }
    // my orders 
    public function myOrders_post(){
       
        $input = json_decode(file_get_contents('php://input'), true);  
        $user_code    = $input['userId'] !="" ? $input['userId']:'';
        $user_token   = $input['user_token']!="" ? $input['user_token']:'';        
        if(!empty($user_code) && !empty($user_token)){
           $checkToken = $this->Adminmodel->checkUserAuthKey($user_code,$user_token);                      
           if($checkToken > 0){ 
            $where = array(
                "isactive"=> 0 ,
                "owner_id" => $user_code
            );        
            $columnList = array("id","group_id","owner_id",'order_id','ship_id','sub_total','discount','service_tax','delivery_charges','grand_total','	payment_mehthod','payment_status','order_status','created_at');
            $result = $this->Adminmodel->fetchAllrecords('order_details',$where,$columnList);
                if($result)
                {
                    foreach ($result as $key => $field) {
                        $result[$key]['order_status']=getOrderStatus($field['order_status']);
                        $result[$key]['ProductList']=replace_empty_values(self::getProductListOrder($field['order_id']));
                        $result[$key]['ProductCount']=count(replace_empty_values(self::getProductListOrder($field['order_id'])));
                    }                       
                    $json_data['status']            ='TRUE';
                    $json_data['responseCode']      = 0;
                    $json_data['message']           = array("Order  detail retrieved successfully.");
                    $json_data['orderList']         = replace_empty_values($result);
                    $json_data['shippingDetail'] = (object)replace_empty_values(self::getShippingAddress($field['ship_id']));                    
                    $this->set_response($json_data,REST_Controller::HTTP_OK); 
                }
                else
                {
                    $responseList = self::success_response('FALSE','1',MESSAGE_NORECORD);
                    $this->set_response($responseList,REST_Controller::HTTP_OK); 
                }
            }
            else{
                    $responseList = self::success_response('FALSE','12',MESSAGE_INVALDTOKEN);
                    $this->set_response($responseList,REST_Controller::HTTP_OK);
            }
        }    
        else{
            $responseList = self::success_response('FALSE','2',MESSAGE_PARAMETERMISSING);
            $this->set_response($responseList,REST_Controller::HTTP_OK); 
        }
    }
    
    // my single order 
    
    public function mySingleOrders_post(){
        $input = json_decode(file_get_contents('php://input'), true);  
        $user_code    = $input['userId'] !="" ? $input['userId']:'';
        $user_token   = $input['user_token']!="" ? $input['user_token']:'';  
        $order_id     = $input['order_id']!="" ? $input['order_id']:'';    
        if(!empty($user_code) && !empty($user_token) && !empty($order_id)){
           $checkToken = $this->Adminmodel->checkUserAuthKey($user_code,$user_token);                      
           if($checkToken > 0){ 
                $where = array(
                "isactive"=> 0 ,
                "owner_id" => $user_code,
                "order_id" => $order_id
                );        
                $columnList = array("id","group_id","owner_id",'order_id','ship_id','sub_total','discount','service_tax','delivery_charges','grand_total','	payment_mehthod','payment_status','order_status','created_at');
                $result = $this->Adminmodel->fetchAllrecords('order_details',$where,$columnList);
                if($result)
                {
                foreach ($result as $key => $field) {
                    $result[$key]['order_status']=getOrderStatus($field['order_status']); 
                    $result[$key]['ProductList']=replace_empty_values(self::getProductListOrder($field['order_id']));
                }                    
                $json_data['status']            ='TRUE';
                $json_data['responseCode']      = 0;
                $json_data['message']           = array("Order  detail retrieved successfully.");
                $json_data['orderList']         = replace_empty_values($result);
                $json_data['shippingDetail'] = (object)replace_empty_values(self::getShippingAddress($field['ship_id']));                 
                $this->set_response($json_data,REST_Controller::HTTP_OK); 
                }
                else
                {
                    $responseList = self::success_response('FALSE','1',MESSAGE_NORECORD);
                    $this->set_response($responseList,REST_Controller::HTTP_OK); 
                }
            }
            else{
                    $responseList = self::success_response('FALSE','12',MESSAGE_INVALDTOKEN);
                    $this->set_response($responseList,REST_Controller::HTTP_OK);
            }
        }    
        else{
            $responseList = self::success_response('FALSE','2',MESSAGE_PARAMETERMISSING);
            $this->set_response($responseList,REST_Controller::HTTP_OK); 
        }
    }
    public function getProductListOrder($id){
        $where = array(
            "order_id" => $id
        );        
        $columnList = array("id","group_id","owner_id",'order_id','product_id','product_weight','product_price','product_quantity','vendor_id','branch_id');
        $result = $this->Adminmodel->fetchAllrecords('order_product_details',$where,$columnList);
        if($result){
            foreach ($result as $key => $field) {
                    $result[$key]['productName'] = self::getProductName($field['product_id']);
                     $result[$key]['sizeName']   = self::getIWeight($field['size']);
                     $result[$key]['image']   =  base_url()."/uploads/product_image/".self::getImageName($field['product_id']);
                     
                 }   
            return $result;
        }
        else{
            $array = array();
            return $array ;
        }
    }
    //shipping address
    public function getShippingAddress($id){
        $where = array('id'=> $id);
        $columnList = array("id","user_id","house_no",'street_no','area','city','land_mark','address_type');
        $result = $this->Adminmodel->fetchAllrecords('address_list',$where,$columnList);
        if($result){
            foreach ($result as $keynew => $fieldnew) {
                if($fieldnew['address_type']==1){
                    $address_type ="Home";
                }
                elseif($fieldnew['address_type']==2){
                    $address_type ="Office";
                }
                elseif($fieldnew['address_type']==3){
                    $address_type ="Other";
                }
                else{
                    $address_type ="Other";
                }
                $result[$keynew]['address_type']= $address_type ;
            }
            return $result;
        }
        else{
            $array = array();
            return $array ;
        }
    }
    public function getProductName($id){
        $this->db->where('id',$id);
        $query = $this->db->get('vendor_products');
        $result = $query->row();
        if($result){
            return $result->product_name;
        }
        else{
            return "" ;
        }
    }
    public function getImageName($id){
        $this->db->where('id',$id);
        $query = $this->db->get('vendor_products');
        $result = $query->row();
        if($result){
            return $result->product_image;
        }
        else{
            return "" ;
        }
    }
    public function getIWeight($id){
        $this->db->where('id',$id);
        $query = $this->db->get('prodcut_weights');
        $result = $query->row();
        if($result){
            return $result->product_weight;
        }
        else{
            return "" ;
        }
    }

    // for success response json 
    public function success_response($status,$responseCode,$message,$data=null){
        $json_data['status']=$status;
        $json_data['responseCode']=$responseCode;
        $json_data['message']=$message;
        if($data !=""){
            $json_data['list']=$data;
        }
        return $json_data ;
    }
    public function success_responseSingle($status,$responseCode,$message,$columnName,$columnValue){
        $json_data['status']=$status;
        $json_data['responseCode']=$responseCode;
        $json_data['message']=$message;
        if($columnValue !="" && $columnName !=""){
            $json_data[$columnName]=$columnValue;
        }
        return $json_data ;
    }
    
}   
?>