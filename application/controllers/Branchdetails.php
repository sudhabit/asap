<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Branchdetails extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper("encryptionpwd");
        $this->load->library('form_validation');
        $this->load->model('Adminmodel');
        $this->load->library("pagination");
    }
    public function branchDetails(){
        if(!is_branchlogged_in())  // if you add in constructor no need write each function in above controller.
        {
        redirect('Masterbranch');
        }
        $input  = json_decode(file_get_contents('php://input'), true);
        $start=0;
        $perPage = 100;
        $branch = $this->session->userdata('branchCode');
        $branchId=$this->Adminmodel->getSingleColumnName($branch,'branch_code','id','vendor_branch_details');
        $table="vendor_branch_details";
        if($branchId !=""){
            @$column = "id";
            @$value  = $branchId;
        }
        $search ='';
    
        $result1 = $this->Adminmodel->get_current_page_records($table,$perPage,$start,@$column,@$value,@$search,@$searchColumn);
        $result=replace_attr($result1);
        if($result){
            foreach ($result as $key => $field) {
                $data['cancelled_cheque'] = base_url()."uploads/cancelled_cheque/".$field['cancelled_cheque'];
            }
            $data['result'] = $result[0] ;
            $this->load->view('branch/branch_Details',$data);
        }
        else{
        $url='viewBranch';
        redirect($url);
        }
    }
    
   
}
?>

