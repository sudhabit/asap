<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('sendMobileSMS')){
    function sendMobileSMS($message,$user_mobile) {
        global $conn;
        //Your authentication key
        $authKey = "224221AVqPi4m6cPTu5b3cacf1";
        //Multiple mobiles numbers separated by comma
        $mobileNumber =$user_mobile;
        //Sender ID,While using route4 sender id should be 6 characters long.
        $senderId = "FOOMLY";
        //Your message to send, Add URL encoding here.
        $message =$message;
        //Define route 
        $route = "4";
        //Prepare you post parameters
        $postData = array(
        'authkey' => $authKey,
        'mobiles' => $mobileNumber,
        'message' => $message,
        'sender' => $senderId,
        'route' => $route
        );
        //API URL
        $url="http://api.msg91.com/api/sendhttp.php";
        // init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POST => true,
        CURLOPT_POSTFIELDS => $postData
        //,CURLOPT_FOLLOWLOCATION => true
        ));
        //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        //get response
        $output = curl_exec($ch);
        //Print error if any
        if(curl_errno($ch))
        {
        echo 'error:' . curl_error($ch);
        }
        curl_close($ch);         
        }

}
if (!function_exists('getOrderStatus')){

    function getOrderStatus($id){
        switch ($id) {
            case "1":
                return  "new order";
                break;
                case "2":
                return  "Completed";
                break;
            case "3":
                return  "Cancelled";
                break;
            case "4":
                return  "In Process";
                break;
            case "5":
                return  "Delivered";
                break;
            case "0":
                return  "need to confirm";
                break;
            default:
            return  "";
        }
    }

}