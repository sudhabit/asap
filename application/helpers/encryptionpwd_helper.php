<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('encryptPassword'))
{
     function encryptPassword($pwd) {
        $key = "123";
        $admin_pwd = @bin2hex(openssl_encrypt($pwd,'AES-128-CBC', $key));
        return $admin_pwd;
    }
}
if (!function_exists('decryptPassword'))
{
    function decryptPassword($pwd) {
        $key = "123";
        $admin_pwd = openssl_decrypt(hex2bin($pwd),'AES-128-CBC',$key);
        return $admin_pwd;
    }
} 
function replace_empty_values($arr){
    foreach($arr as &$val){
    if(is_array($val)) $val = replace_empty_values($val);
    else if($val === "" || $val === false || $val === null) $val = "0";
    }
return $arr;
}
//To Logged in -- HR
if (!function_exists('is_logged_in'))
{

   function is_logged_in() {
   $ci = &get_instance();

     //load the session library
     $ci->load->library('session');

     $user =$ci->session->userdata('isUserLoggedIn');
     if (!isset($user)) {
      return false;
     }
    else {
      return true;
    }
   }
}
//To Vendor Logged in -- HR
if (!function_exists('is_vendorlogged_in'))
{

   function is_vendorlogged_in() {
   $ci = &get_instance();

     //load the session library
     $ci->load->library('session');

     $user =$ci->session->userdata('isVendorLoggedIn');
     if (!isset($user)) {
      return false;
     }
    else {
      return true;
    }
   }
}
//To Branch Logged in -- HR
if (!function_exists('is_branchlogged_in'))
{

   function is_branchlogged_in() {
   $ci = &get_instance();

     //load the session library
     $ci->load->library('session');

     $user =$ci->session->userdata('isBranchLoggedIn');
     if (!isset($user)) {
      return false;
     }
    else {
      return true;
    }
   }
}
//attribute for empty values -- HR
function replace_attr($arr){
    foreach($arr as &$val){
        if(is_array($val)) $val = replace_attr($val);
        else if($val === "0" || $val === false || $val === null || $val === "") $val = "--";
    }
    return $arr;
}